
// Init booker
function booker_init(){

	// Get the book name
	book = Cala.paramsGet('book', false);
	if(!book){
		Cala.say('No book found');
        Cala.iGoTo("index.html?src=noBookFound");
	}else{
		bookPath = 
		booker_openBook(book);
	}
}

// Load a book
function booker_openBook(book){
	bookPath = book + "/" + book + ".html";
	Cala.say("Opening a book: " + bookPath);
	$("#bhavana_viewBook").load('modules/booker/booksHTML/'+bookPath+'?ppp=' + Math.floor(Math.random() * 1000));
}
