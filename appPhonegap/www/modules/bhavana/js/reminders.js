Bhavana_setReminders = {

    // The random reminders all have an id of 1000+
    start: 0,
    end: 0,
    q: 0,
    interval: 0,
    minutes: 0, // interval in minutes
    allTimes: [],
    missing: false, // Are there any missing details
    sound: "file://modules/bhavana/bell_1.mp3", // There will be more soon...
    dailyId: 2001, // The id of the daily reminder

    // Reloads this page after changes are done
    reload: function(){
        Cala.say("Refreshing");
        Cala.iGoTo("?x=bhavava/reminders");
        return this;
    },
    // I will create a new set of reminders each day
    setRandomRemindersCreator: function(){
        Cala.say("Storing information to create new reminders each day");

        this.missing = false;

        // Set the numbers
        this.start = ($("#bhavana_start").val() === "" || $("#bhavana_start").val() === undefined) ? 7 : $("#bhavana_start").val();
        this.end   = ($("#bhavana_end").val() === "" || $("#bhavana_end").val() === undefined) ? 8 : $("#bhavana_end").val(); //$("#bhavana_end").val();
        this.q     = ($("#bhavana_q").val() === "" || $("#bhavana_q").val() === undefined) ? 5 : $("#bhavana_q").val(); //$("#bhavana_q").val();

        // Am/pm for ini
        var iniAmPm = document.querySelector('input[name="randomIniAmPm"]:checked').value;
        if(iniAmPm == "pm"){
            this.start = parseInt(this.start) + 12;
        }

        // Am/pm for end
        var endAmPm = document.querySelector('input[name="randomEndAmPm"]:checked').value;
        if(endAmPm == "pm"){
            this.end = parseInt(this.end) + 12;
        }

        Cala.say("Start at hour: " + this.start + " am/pm: " + iniAmPm);
        Cala.say("End at hour: " + this.end + " am/pm: " + endAmPm);

        // Start before end
        if(parseInt(this.end) < parseInt(this.start)){
            Cala.danger("La hora de inicio debe ser posterior a la hora de fin.", 5);
            this.missing = true;
            return this;
        }

        if(this.start === "" || this.end === "" || this.q === ""){
            Cala.danger("Faltan datos", 3);
            this.missing = true;
            return this;
        }

        this.interval = this.end - this.start; // total hours
        this.minutes = this.interval*60;

        Cala.say("Interval: " + this.interval + " mins: " + this.minutes);

        Cala.say("Setting " + this.q + " numbers between: " + this.start + " and " + this.end);

        Cala.say("Storing the information for later");

        Cala.Keys.store("remindersStartTime", this.start);
        Cala.Keys.store("remindersEndTime",   this.end);
        Cala.Keys.store("remindersInterval",  this.interval);
        Cala.Keys.store("remindersMinutes",   this.minutes);
        Cala.Keys.store("remindersQ",         this.q);

        // Set a reminder to do this every day
            cordova.plugins.notification.local.schedule({
                id: 7000,
                title: "¿Cómo está su mente?",
                text: "Recuerde respirar, cada momento cuenta. Hága un registro de su mente ahora y siga su progreso.",
                every: "day",
                at: newReminderFirstDate,
                sound: this.sound
            });


        return this;

    },
    // I will actually set the random reminders for today
    setRemindersForToday: function(){
        Cala.say("Setting today's reminders");

        // Lets get the information
        this.start    = Cala.Keys.get("remindersStartTime", 7);
        this.end      = Cala.Keys.get("remindersEndTime",   20);
        this.interval = Cala.Keys.get("remindersInterval",  13);
        this.minutes  = Cala.Keys.get("remindersMinutes",   780);
        this.q        = Cala.Keys.get("remindersQ",         5);

        for(var i = 0; i < this.q; i++){

            Cala.say("Setting: " + i);

            // Get a moment bewteen 0 and the interval in minutes
            newTime = Math.floor(Math.random() * this.minutes) + 1;
            // if that moment already happened, I will ignore it
            if(this.allTimes.indexOf(newTime) === -1){
                Cala.say("Found a new time..." + newTime);
                this.allTimes.push(newTime);
            }else{
                Cala.say("I have this one already");
                // Don't count this round
                i--;
            }
        }

        this.setTimesRandom();

        return this;

    },
    // Checks if there are set random reminders
    checkRandom: function(){
        Cala.say("Looking for random reminders");
        // Do something
        if(Cala.Keys.get('bhavana_remindersRandomSet', 0) == 1){
            Cala.say("Found some random ones");
            $("#bhavana_remindersRandomCreate").hide('slow', function(){
                $("#bhavana_remindersRandomClear").show();
            });
        }else{
            Cala.say("No random notifications found");
            $("#bhavana_remindersRandomCreate").show('slow', function(){
                $("#bhavana_remindersRandomClear").hide();
            });
        }

        return this;
    },
    // Calculates random reminders
    calculateRandom: function(){

        this.missing = false;

        // Set the numbers
        this.start = ($("#bhavana_start").val() === "" || $("#bhavana_start").val() === undefined) ? 7 : $("#bhavana_start").val();
        this.end   = ($("#bhavana_end").val() === "" || $("#bhavana_end").val() === undefined) ? 8 : $("#bhavana_end").val(); //$("#bhavana_end").val();
        this.q     = ($("#bhavana_q").val() === "" || $("#bhavana_q").val() === undefined) ? 5 : $("#bhavana_q").val(); //$("#bhavana_q").val();

        // Am/pm for ini
        var iniAmPm = document.querySelector('input[name="randomIniAmPm"]:checked').value;
        if(iniAmPm == "pm"){
            this.start = parseInt(this.start) + 12;
        }

        // Am/pm for end
        var endAmPm = document.querySelector('input[name="randomEndAmPm"]:checked').value;
        if(endAmPm == "pm"){
            this.end = parseInt(this.end) + 12;
        }

        Cala.say("Start at hour: " + this.start + " am/pm: " + iniAmPm);
        Cala.say("End at hour: " + this.end + " am/pm: " + endAmPm);

        // Start before end
        if(parseInt(this.end) < parseInt(this.start)){
            Cala.danger("La hora de inicio debe ser posterior a la hora de fin.", 5);
            this.missing = true;
            return this;
        }

        if(this.start === "" || this.end === "" || this.q === ""){
            Cala.danger("Faltan datos", 3);
            this.missing = true;
            return this;
        }

        this.interval = this.end - this.start; // total hours
        this.minutes = this.interval*60;

        Cala.say("Interval: " + this.interval + " mins: " + this.minutes);

        Cala.say("Setting " + this.q + " numbers between: " + this.start + " and " + this.end);

        for(var i = 0; i < this.q; i++){

            Cala.say("Setting: " + i);

            // Get a moment bewteen 0 and the interval in minutes
            newTime = Math.floor(Math.random() * this.minutes) + 1;
            // if that moment already happened, I will ignore it
            if(this.allTimes.indexOf(newTime) === -1){
                Cala.say("Found a new time..." + newTime);
                this.allTimes.push(newTime);
            }else{
                Cala.say("I have this one already");
                // Don't count this round
                i--;
            }
        }
        return this;
    },
    // Clear all random times
    randomClearAll: function(){
        Cala.say("Clearing all random alerts");

        Cala.Keys.remove('bhavana_remindersRandomSet');

        // Change visuals
        $("#bhavana_remindersRandomClear").hide('slow', function(){
            $("#bhavana_remindersRandomCreate").show('slow');
        });

        // I will just clear A LOT of them, i don't think anyone will ever use this much, lets hope
        for(var j = 1000; j < 1100; j++){
            cordova.plugins.notification.local.clear(j);
        }
        return this;
    },

    // This actually sets the times at random
    setTimesRandom: function(){

        // Where there any missing values?
        if(this.missing === true){
            Cala.say("Unable to set new timers");
            return this;
        }
        Cala.say("Setting the reminders");
        var today = new Date(); //.getTime();

        var todayStartDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0, 0);

        Cala.say("Today started at: " + todayStartDate);

        // Store in local storage
        Cala.Keys.store('bhavana_remindersRandomSet', 1);

        // Change visuals
        $("#bhavana_remindersRandomCreate").hide('slow', function(){
            $("#bhavana_remindersRandomClear").show('slow');
        });

        for(var i = 0; i < this.allTimes.length; i++){
            // New time will be the minutes + the start time
            thisTime = ((this.start*60) + this.allTimes[i])/60;
            // Hours
            hours = Math.floor(thisTime);
            minutes = Math.floor((thisTime - hours) * 60);
            Cala.say("Time is at this hours:" + hours + " and minutes: " + minutes + " ->> " + thisTime);
            // Create the correct first date
            var newReminderFirstDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), hours, minutes, 0, 0);
            Cala.say("The full time will be: " + newReminderFirstDate);

            // All this reminders start with 1000
            cordova.plugins.notification.local.schedule({
                id: parseInt(i)+1000,
                title: "¿Cómo está su mente?",
                text: "Recuerde respirar, cada momento cuenta. Hága un registro de su mente ahora y siga su progreso.",
                every: "day",
                at: newReminderFirstDate,
                sound: this.sound
            });
        }

        Cala.success("Se han creado los recordatorios.", 5);

        return this;
    },
    // Set a daily reminder
    setDaily: function(){

        Cala.say("Creating a daily reminder to meditate at: " + $("#bhavana_daily").val());

        // Get the hour
        var hour = ($("#bhavana_daily").val() === undefined || $("#bhavana_daily").val() === "") ? 7 : $("#bhavana_daily").val();

        if(hour === undefined || hour === ""){
            Cala.danger("¿A que hora desea que le recuerde?", 3);
            return false;
        }

        // Am/pm
        var ampm = document.querySelector('input[name="dailyAmPm"]:checked').value;
        if(ampm == "pm"){
            hour = parseInt(hour) + 12;
        }

        Cala.say("hour: " + hour + " am/pm: " + ampm);

        var today = new Date();

        // Create the correct first date
        var newReminderFirstDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), hour, 0, 0, 0);
        Cala.say("The full time will be: " + newReminderFirstDate + " and id: " + this.dailyId);

        // Set a local storage
        $("#bhavana_remindersDailyCreate").hide('slow', function(){
            $("#bhavana_remindersDailyClear").show('slow');
        });

        // Store in local storage
        Cala.Keys.store('bhavana_remindersDaily', hour);

        this.dailySetInfo(hour);

        cordova.plugins.notification.local.schedule({
            id: this.dailyId,
            title: "¿Ya meditó el día de hoy?",
            text: "Recuerde respirar, cada momento cuenta. Registre su mente ahora.",
            every: "day",
            at: newReminderFirstDate,
            sound: this.sound
        });

        Cala.info("Todo listo.", 3);

        return this;
    },
    // Clear the daily reminder
    clearDaily: function(){
        Cala.say("Clearing daily reminder");

        Cala.Keys.remove('bhavana_remindersDaily');

        $("#bhavana_remindersDailyClear").hide('slow',function(){
            $("#bhavana_remindersDailyCreate").show('slow');
        });

        // Remove from local storage
        cordova.plugins.notification.local.cancel(this.dailyId, function(){
            Cala.say("Removed the daily reminders");
        });
        return this;
    },
    // Displays the information about the daily reminder
    dailySetInfo: function(time){
        Cala.say("Displaying info about daily reminder");
        // If over 12
        if(time > 12){
            Cala.say("Time is over 12pm");
            time = (time - 12) + "pm";
        }else{
            time = time + "am";
        }
        $("#bhavana_remindersDailyCurrent").html(time);
    },
    // Check if the daily reminder is set
    checkDailyIsSet: function(){

        Cala.say("Looking for daily reminders");

        // This is 2001
        var dailyReminderTime = Cala.Keys.get('bhavana_remindersDaily', 0);

        if(dailyReminderTime !== 0){
            this.dailySetInfo(dailyReminderTime);
            Cala.say("The daily reminder is set");
            $("#bhavana_remindersDailyCreate").hide();

        }else{
            $("#bhavana_remindersDailyClear").hide();
            Cala.say("The daily reminder is NOT set");
        }

        return this;
    },

    // Get a list of scheduled reminders
    getScheduledAll: function(){
        Cala.say("Getting scheduled ids");
        cordova.plugins.notification.local.getScheduledIds(function(ids){
            Cala.say("Found some set notifications" + JSON.stringify(ids));
            return ids;
        });
        // This should never happen
        var ids = [];
        return ids;
    }
};

function scheduleMultiple() {

    var sound = "file://modules/bhavana/bell_1.mp3";

    Cala.say("Scheduling multiple with audio: " + sound);

    cordova.plugins.notification.local.schedule({
        id: 41,
        title: 'Scheduled with delay',
        text: 'Test Message 1',
        sound: sound
    });

    Cala.info("Todo listo.", 3);

    /* 
       cordova.plugins.notification.local.schedule([{
       id: 1,
       text: 'Multi Message 1 porque si',
       sound: Cala_createAudioPath() + "modules/bhavana/bell_1.mp3"
       }, {
       id: 2,
       text: 'Multi Message 2 porque también'
       }, {
       id: 3,
       text: 'Multi Message 3 y entonces todo bien, espero'
       }]);*/
}


//bhavana_setReminders.calculate().setTimes();
//bhavana_setReminders.setDaily();

