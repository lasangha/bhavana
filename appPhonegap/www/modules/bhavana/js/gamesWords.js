//http://www.creandotuvida.com/Vocabulario_Positivo.html
var SelfEsteam = {
    positiveWords: ['amor','felicidad','paz','compasión','óptimo','genuino','leal',
    'equitativo','valiente','caritativo','comprensivo','bueno','bonachón','altruista',
    'admirable','maravilloso','agradable','hermoso','compasivo','piadoso','sensible',
    'generoso','bondadoso','honesto','afectuoso','divertivo','tierno','simple','inocente',
    'beneficioso','conveniente','propicio','justo','excelente','generoso','desprendido',
    'dadivoso','caritativo','benéfico','abundante','productivo','noble','honroso','sublime',
    'desinteresado','sincero','leal','franco','digno','estimable','apreciable','sincero','claro',
    'espontáneo','natural','verdadero','real','ingenuo','cordial','honesto','decente','digno',
    'íntegro','sincero','justo','querido'],
    negativeWords: ['tristeza','preocupación','soledad','duda','angustia','soledad','negación',
    'malo','feo','horroso','crítica','maldad','desconsuelo','abandono','golpear','agresión',
    'maltrato','injusticia','avaricia','apego','gritar'],
    totalPoints: 0,
    newPointsBar: 5,
    running: true,
    // Gets a new word
    getNewWord: function(){
        newWord = '';
        Cala.say("Getting a new word");
        // Possitive or negative
        posNeg = Math.floor(Math.random() * 10);
        if(posNeg < 5){
            Cala.say("Got a negative word: " + posNeg);
            wordPos = Math.floor(Math.random() * (this.negativeWords.length-1));
            newWord = this.negativeWords[wordPos];
            points = 0;
        }else{
            Cala.say("Got a positive word: " + posNeg);
            wordPos = Math.floor(Math.random() * (this.positiveWords.length-1));
            newWord = this.positiveWords[wordPos];
            points = 1;
        }
        Cala.say("Got word: " + newWord);
        $(".wordsGame").last().remove();
        $("#allWords").prepend("<li onCLick='SelfEsteam.sum("+points+");' class='list-group-item wordsGame' style='text-align: center;'><h1>"+newWord+"</h1></li>");

        // Set a timer in 1 second
        if(this.running === true){
        Cala.say("One more time...");
            setTimeout(function(){SelfEsteam.getNewWord();}, 1000);
        }
    },
    // Sums points to the game
    sum: function(q){
        Cala.say("Adding points: " + q);
        if(q > 0){
            Cala.say("Got points");
            this.totalPoints = this.totalPoints + 1;
            $("#totalPoints").html(this.totalPoints);
            if(this.totalPoints >= this.newPointsBar){
                Cala.danger("Muy bien!!!", 3);
                this.newPointsBar = this.newPointsBar * 2;
                $("#totalPointsNextBar").html(this.newPointsBar);
            }
        }else{
            Cala.say("Adding more points for next target");
            this.newPointsBar = this.newPointsBar + 1;
            $("#totalPointsNextBar").html(this.newPointsBar);
        }
    },
    start: function(){
        Cala.say("Starting/resuming the game");
        this.running = true;
        setTimeout(function(){SelfEsteam.getNewWord();}, 1000);
        $("#playGame").hide('slow', function(){$("#pauseGame").show('slow');});
    },

    pause: function(){
        Cala.say("Pausing the game...");
        this.running = false;
        $("#pauseGame").hide('slow', function(){$("#playGame").show('slow');});
    }
};

//setTimeout(function(){SelfEsteam.getNewWord();}, 1000);
//SelfEsteam.getNewWord();

