// General things

// Total time in meditation for this session
var bhavana_meditationTotalTime = 0;

/****************************************************************************
 *
 * Navigation
 *
 */

function Bhavana_storeThisPage(){

	// Re set this page
	thisPage = document.URL;

	Cala.say("Storing this page: >>>>> " + thisPage);

	// Is this an storable page?
	if(Cala.paramsGet("x", "") != "bhavana/sessions"){
		Cala.say("I shall not store this page!");
		return false;
	}
	else{
		Cala.Keys.store("var_bhavana_lastPage", thisPage);
	}
}

/**
 * Alert if this is a feature available only for the app
 */
function Bhavana_alertAppOnlyFeature(){
    if(Cala.onApp === false){
        Cala.info('Esta función solo está disponible para el App, adquiérala gratis en Android! <br> <a href="http://android">Android!</a>' +
                '<a href="https://play.google.com/store/apps/details?id=org.lasangha.bhavana" onClick="return Cala_externalLink(\'playApp\');" id="playApp">' +
                '<img alt="Get it on Google Play"' +
                'src="https://developer.android.com/images/brand/es-419_generic_rgb_wo_45.png" /></a>');
    }
}

/****************************************************************************
 *
 * Meditations
 *
 */

// Add meditation times to the causes
function Bhavana_addToCause(_totalTime, _causeCode, _ref){

    Cala.say("I will submit this time to the causes?");

    // Lets see first if the user wants to participate in global meditations
    var privacyOption = Cala.Keys.get("bhavana_privacyGlobalMeditations", 1);

	// New system
	$("#bhavana_register").html('<iframe src="' + Cala.ip + 'bhavana/meditationAddTime/' +
			_totalTime + '/' + _causeCode + '/' + privacyOption + '/' + (Cala.onApp === true ? 'app' : 'web') + '/' +_ref +
			'" width="0" height="0"></iframe>');

	// Old system
	/*
    var myData = {causeCode: _causeCode, totalTime: _totalTime, privacy: privacyOption, where: (Cala.onApp === true ? 'app' : 'web'), ref: _ref};
    Drupal.services.call({
        method: 'POST',
        path: 'bhavana_resources/meditation_add_times.json',
        data: JSON.stringify(myData),
        success: function(result) {
            Cala.say("Result: " + result);
        }
    });
	*/

}

// I retrieve the medatitation with most meditated time
function Bhavana_getMeditationMaxCauseTime(){

	Cala.say("I will get the cause with more minutes meditated...");

	Cala.say("Getting times");

	bhavana_serverCallGet('bhavana/getMaxTime', function(result){
		Cala.say("Got some info about those times" + JSON.stringify(result));
		$("#causesTotalMinutes").html(result.totalTime);
		$("#causesCause").html(result.name);
	});
	/*
	//Lets make the call
	Drupal.services.call({
	method: 'POST',
	path: 'bhavana_resources/causes_get_max_time.json',
	success: function(result) {
	Cala.say("Result: " + result);
	if(typeof result == 'object'){
	Cala.say("Got information about the times");
	$("#causesTotalMinutes").html(result.totalTime);
	$("#causesCause").html(result.name);
	Cala.say(result.totalTime);
	}
	},
	error: function(){
	Cala.say("Something wrong?");
	}
	});
	*/
}

// Calculate and see if there is new time to register
function Bhavana_timeRegister(currentTime){

	// Only if this is more than 1 minute
	if(currentTime >= 1){
		totalTime = currentTime - bhavana_meditationTotalTime;
		Cala.say("Time is: " + currentTime + " last: " + bhavana_meditationTotalTime + " total: " + totalTime);

		// This should be given in minutes, I wil only do it if it is more than 1
		if(totalTime < 1){
			Cala.say("Not enought time");
			return false;
		}

		// Bring time up to speed
		bhavana_meditationTotalTime = currentTime;

		return totalTime;

	}else{
		Cala.say("Less than a minute");
		return false;
	}

	return false;

}

/****************************************************************************
 *
 * Privacy Settings
 *
 * 1: yes, I want to participate
 * 2: no, I do not want to participate
 * By default it is yes
 */

// Stores the privacy settings for this person in this device
function Bhavana_privacyGlobalSet(){

	//Which setting was selected
	var privacyOption = $("#bhavana_privacyGlobalMeditations").val();
	Cala.say("User selected privacy option: " + privacyOption);
	Cala.Keys.store("bhavana_privacyGlobalMeditations", privacyOption);
	Cala.info("Cambios hechos");
}

// Gets the privacy settings for this person in this device
function Bhavana_privacyGlobalGet(){

	//Which setting was selected
	var privacyOption = Cala.Keys.get("bhavana_privacyGlobalMeditations", 1);
	$("#bhavana_privacyGlobalMeditations").val(privacyOption);
}

/****************************************************************************
 *
 * Sessions
 *
 */

// I will take you to the last visited page of the course
function Bhavana_gotoLastSessionPage(){

	// Where I'm I?
	var thisPage = document.URL;

	Cala.say("I am in page: " + thisPage);

	// Where was I?
	var lastValue = Cala.Keys.get("var_bhavana_lastPage", "startMeOver");

	// If there is a last location, I'll go there
	if(lastValue === undefined || lastValue == "startMeOver"){
		Cala.say("Nothing set");
		Cala.iGoTo("?x=bhavana/c_intro");
	}
	else{
		Cala.say("Going to: " + lastValue);
		Cala.iGoTo(lastValue);
	}

	return false;

}

/****************************************************************************
 *
 * Reports and Charts
 *
 */

function Bhavana_createChart(chartTitle, canvasId, chartLabels, chartData, type){

	Cala.say("Creating chart");

	if(chartLabels.length === 0){
		Cala.say("No information");
		Cala_chartsNoInfo("Bhavana_chartTotalPerDayMeDiv", "");
	}else{

		Cala.say("There are some meditations: " + chartLabels.length);

		var lineChartData = {
			labels : chartLabels,
			datasets : [
			{
				label: chartTitle,
				fillColor: "rgba(76,153,0,0.2)",
				strokeColor: "rgba(76,153,0,1)",
				pointColor: "rgba(76,153,0,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke: "rgba(76,153,0,1)",
				data : chartData
			}
			]
		};

		var ctx = document.getElementById(canvasId).getContext("2d");
		window.myLine = new Chart(ctx).Line(lineChartData, {responsive: true});
	}
}

// Create a 'no results' message in charts
// @deprecated use Cala_chartsNoInfo();
function Bhavana_chartsNoInfoMsg(id, extraMessage){
	Cala_chartsNoInfo(id, extraMessage);
	//Cala.say("No information on: " + id);
	//$(id).html("<div role='alert' class='alert alert-warning'>No hay resultados en este momento</div>");
}

// Get all meditation times per day, for me?
function Bhavana_getAllMeditationTimesPerDay(){

	bhavana_serverCallGet('bhavana/getGroupMeditations', function(resp){
		Cala.say("Got some info about those group times" + JSON.stringify(resp));
		if(resp.dates.length === 0){
			Cala.say("No results");
			$("#groupMeditationsBody").html("<div role='alert' class='alert alert-warning'>No hay resultados en este momento</div>");
		}else{
			var newData = {
				labels: resp.dates,
	datasets: [
	{
		label: "Solo Meditando",
	fillColor: "rgba(231,77,30,0.2)",
	strokeColor: "rgba(231,77,30,1)",
	pointColor: "rgba(231,77,30,1)",
	pointStrokeColor: "#fff",
	pointHighlightFill: "#fff",
	pointHighlightStroke: "rgba(255,255,153,1)",
	data: resp.details.Ninguna
	},
	{
		label: "Paz",
		fillColor: "rgba(255,255,153,0.2)",
		strokeColor: "rgba(255,255,153,1)",
		pointColor: "rgba(255,255,153,1)",
		pointStrokeColor: "#fff",
		pointHighlightFill: "#fff",
		pointHighlightStroke: "rgba(255,255,153,1)",
		data: resp.details.Paz
	},
	{
		label: "Humildad",
		fillColor: "rgba(51,255,153,0.2)",
		strokeColor: "rgba(51,255,153,1)",
		pointColor: "rgba(51,255,153,1)",
		pointStrokeColor: "#fff",
		pointHighlightFill: "#fff",
		pointHighlightStroke: "rgba(51,255,153,1)",
		data: resp.details.Humildad
	},
	{
		label: "Compasión",
		fillColor: "rgba(76,153,0,0.2)",
		strokeColor: "rgba(76,153,0,1)",
		pointColor: "rgba(76,153,0,1)",
		pointStrokeColor: "#fff",
		pointHighlightFill: "#fff",
		pointHighlightStroke: "rgba(76,153,0,1)",
		data: resp.details['Compasi\u00f3n']
	}
	]
			};
			var ctx = document.getElementById("myChart").getContext("2d");
			var options = {
				responsive: true,
				multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
				legendTemplate : "<ul class=\"list-group <%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li class=\"list-group-item\"><span style=\"background-color:<%=datasets[i].strokeColor%>\">&nbsp;&nbsp;</span>&nbsp;<%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
			};
			window.myGroupTimesChart = new Chart(ctx).Line(newData, options);
			document.getElementById("groupTimesChart").innerHTML = myGroupTimesChart.generateLegend();
		}
});


/*
   Drupal.services.call({
   method: 'POST',
   path: 'bhavana_resources/meditation_get_group_times.json',
   data: JSON.stringify({ini: 7}),
   success: function(resp) {
   if(resp.dates.length === 0){
   Cala.say("No results");
   $("#groupMeditationsBody").html("<div role='alert' class='alert alert-warning'>No hay resultados en este momento</div>");
   }else{
   var newData = {
   labels: resp.dates,
   datasets: [
   {
   label: "Solo Meditando",
   fillColor: "rgba(231,77,30,0.2)",
   strokeColor: "rgba(231,77,30,1)",
   pointColor: "rgba(231,77,30,1)",
   pointStrokeColor: "#fff",
   pointHighlightFill: "#fff",
   pointHighlightStroke: "rgba(255,255,153,1)",
   data: resp.details.Ninguna
   },
   {
   label: "Paz",
   fillColor: "rgba(255,255,153,0.2)",
   strokeColor: "rgba(255,255,153,1)",
   pointColor: "rgba(255,255,153,1)",
   pointStrokeColor: "#fff",
   pointHighlightFill: "#fff",
   pointHighlightStroke: "rgba(255,255,153,1)",
   data: resp.details.Paz
   },
   {
   label: "Humildad",
   fillColor: "rgba(51,255,153,0.2)",
   strokeColor: "rgba(51,255,153,1)",
   pointColor: "rgba(51,255,153,1)",
   pointStrokeColor: "#fff",
   pointHighlightFill: "#fff",
   pointHighlightStroke: "rgba(51,255,153,1)",
   data: resp.details.Humildad
   },
   {
   label: "Compasión",
   fillColor: "rgba(76,153,0,0.2)",
   strokeColor: "rgba(76,153,0,1)",
   pointColor: "rgba(76,153,0,1)",
   pointStrokeColor: "#fff",
   pointHighlightFill: "#fff",
   pointHighlightStroke: "rgba(76,153,0,1)",
   data: resp.details['Compasi\u00f3n']
   }
   ]
   };
   var ctx = document.getElementById("myChart").getContext("2d");
   var options = {
   responsive: true,
   multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
   legendTemplate : "<ul class=\"list-group <%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li class=\"list-group-item\"><span style=\"background-color:<%=datasets[i].strokeColor%>\">&nbsp;&nbsp;</span>&nbsp;<%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
   };
   window.myGroupTimesChart = new Chart(ctx).Line(newData, options);
   document.getElementById("groupTimesChart").innerHTML = myGroupTimesChart.generateLegend();
   }
   }
   });
   */
}

// Get my meditation times per day
function Bhavana_getMyMeditationTimesPerDay(){

	Cala.say("Lets get the times");

	Drupal.services.call({
		method: 'POST',
		path: 'bhavana_resources/meditation_get_my_times_per_day.json',
		data: JSON.stringify({ini: 7}),
		success: function(resp) {
			Bhavana_createChart("Meditación por día", "Bhavana_chartTotalPerDayMe", resp.labels, resp.times, 'lines');
		},
		error: function(){
			Cala_chartsNoInfo("Bhavana_chartTotalPerDayMe","");
			Cala.say("Error, Nothing found");
		}
	});

}

// Get my meditation times per cause
function Bhavana_getMyMeditationTimesPerCause(){

	Cala.say("Lets get the meditation chart per cause");

	Drupal.services.call({
		method: 'POST',
		path: 'bhavana_resources/meditation_get_my_times_per_cause.json',
		success: function (response) {
			Cala.say("Got good information for the pie");

			var options = {legendTemplate: "<ul class=\"list-group <%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li class=\"list-group-item\"><span style=\"background-color:<%=segments[i].fillColor%>\">&nbsp;<%=segments[i].value%>&nbsp;</span>&nbsp;<%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"};
			var ctx = document.getElementById('chartPiePerCause').getContext('2d');
			var perCausePieChart = new Chart(ctx).Pie(response, options);
			document.getElementById("Bhavana_meditationTimesPerCauseLegend").innerHTML = perCausePieChart.generateLegend();
		},
		error: function(){
			Cala.say("Error getting information about my times per cause");
		}
	});

}

function Bhavana_createMap(){

	Cala.say("creating the map");

	var styleFunction = function(feature, resolution) {
		style = [new ol.style.Style({
			image: new ol.style.Circle({
				radius: 25,
				fill: new ol.style.Fill({
					color: 'rgba(255, 153, 0, 0.4)'
				}),
				stroke: new ol.style.Stroke({
					color: 'rgba(255, 204, 0, 0.2)',
				width: 1
				})
			})
		})];
		return style;
	};

	var map = new ol.Map({
		target: 'myMap',
		layers: [
		new ol.layer.Tile({
			source: new ol.source.OSM()
		}),
			new ol.layer.Vector({
				source: new ol.source.GeoJSON({
					projection: 'EPSG:3857',
				url: Drupal.settings.site_path + '/files/serveMap.php'
				})
			})
	],
		view: new ol.View({
			center: [0, 0],
		zoom: 1
		}),
	});
}

/****************************************************************************
 *
 * Local notifications
 *
 */
/*
if(Cala.onApp === true){
	// This is currently not working at all
	cordova.plugins.notification.local.on("click", function (notification, state) {
		Cala.say("notificado");
		//alert(notification.id + " was clicked");
	});
}
*/
// Generate a 'For Today' quote and put it in the front page
function bhavana_forTodayGenerate(){

	mainTopics = ['Generosidad', 'Moral', 'Renunciamiento', 'Sabiduría', 'Energía',
			   'Paciencia', 'Verdad', 'Determinación', 'Amor Compasivo', 'Ecuanimidad'];

	// This are based on the month's date
	forToday = [
		'Observe sin juzgar, sin evaluar, es solo estar ahí.',
		'Deje ir, sin aferrarse, sin rechazar, solo dejando ir plenamente, entendiendo.',
		'Preste atención a la respiración, una y otra vez.',
		'Todo pasa, todo es impermanente, todo aquello que es creado, eventualmente deja de ser.',
		'Observe la diferencia entre el que piensa y el pensamiento.',
		'Tome el tiempo que sea necesario para observar como todo pasa. Todo lo que es creado, eventualmente desaparece.',
		'Inicie de nuevo, en cada momento, en cada respiración, siempre es posible iniciar de nuevo.'
			];
	var forTodayTasks = [
	{0: 'Deje ir las barreras, observe aquello que nos une a todos los seres.',
		1: 'El engaño fundamental de la humanidades suponer que Yo estoy aquí y tú estás allí afuera. -Yasutani Roshi'},
	{0: 'Observe sin juzgar, sin evaluar. Con la mente abierta a todo aquello que suceda.',
		1: 'La habilidad de observar sin evaluar es la forma más elevada de inteligencia -Krishnamurti'},
	{0: 'Deje ir el pasado, no limite el futuro con ataduras, esté presente en cada momento y sea libre para ser feliz.',
		1: 'Así como una serpiente cambia de piel, debemos dejar atrás el pasado una y otra vez -Buda'},
	{0: 'Observe sus problemas, estúdielos, sin juzgarlos, sin cuestionarlos, simplemente, obsérvelos. ¿Cuáles son reales? ¿Cuáles imaginarios?',
		1: 'El hombre no está preocupado por los problemas reales tanto como por sus ansiedades imaginadas sobre los problemas reales. - Epictelo'},
	{0: 'Observe su mente, en todo momento, en toda respiración, con cada movimiento, solo observe y esté atento a lo que pasa por ella.',
		1: 'Vigile sus pensamientos; estos se convierten en palabras. Vigile sus palabras; estos se convierten en acciones. Vigile sus acciones; estos se convierten en hábitos. Vigile sus hábitos; estos se convierten en carácter. Vigile su carácter; este se convierte en su destino." - Upanishads'},
		{0: 'Reconozca sus estados de animo, no huya de ellos, acéptelos y déjelos ir tranquilamente, haga la paz con ellos.',
			1: 'Cuando comienzas a escuchar el sonido del silencio, es una señal de vacío - del silencio de la mente. Es algo a lo que siempre puedes recurrir. - Ajahn Sumedho'},
		{0: 'Haga una pausa antes de reaccionar, tome su tiempo para observar su mente y responder con calma ante las situaciones del día.',
			1: 'Los sentimientos vienen y van como nubes en un cielo ventoso. La respiración consciente es mi ancla. - Thích Nhất Hạnh'}];

	// Get today's day of the month
	var date = new Date();
	var dayOfWeek = date.getDay();
	Cala.say("Today is day number: " + dayOfWeek);
	Cala.say("The quote is: " + forTodayTasks[dayOfWeek][0]);
	$("#bhavana_forToday").html(forTodayTasks[dayOfWeek][0]);
	$("#bhavana_forTodayQuote").html(forTodayTasks[dayOfWeek][1]);
	$("#bhavana_forTodayMain").html(mainTopics[dayOfWeek]);
	$("#bhavana_forTodayContainer").hide();
}

/**
 * Top menu circular
 * @todo ?? Deprecated?
 */
$(function(){
	//$('a[title]').tooltip();
});

/* Server calls */
function bhavana_serverCallGet(path, _success){

	$.get(Cala.ip + '/' + path)
		.done(function(data){
			_success(data);
			//alert("Data Loaded: " + data);
		});
}

/***************************************************************************
 *
 * Weekly meditation
 *
 */

/**
 * Get the weekly meditations list
 */
function Bhavana_weekMeditationGet(){

	Cala.workingOn.start();

	Cala.say("Getting the script");

	var weeklyTpl = "<div class='card'><div class='card-header'><h2>{name}</h2></div>" +
		"<div class='card-body'>" +
		"<p class='card-text'>{desc}</div>" +
		"<button type='button' class='btn btn-success' onClick='Bhavana_weekMeditationOpen(\"{code}\", \"{name}\")'>Cargar <i class='fa fa-eye' aria-hidden='true'></i></button>" +
		"</div></div><hr />";

	$.getScript(Cala.ip + "bhavana_weekly.js", function(data, textStatus, jqxhr){

		Cala.say("Got the weekly list: " + JSON.stringify(bhavana_weekly));

		for(var item in bhavana_weekly){
			Cala.say("Adding meditation to the list" + JSON.stringify(bhavana_weekly[item]));
			$("#bhavana_weeklyList").append(parseTpl(weeklyTpl, bhavana_weekly[item], true));
			/*
			   $("#bhavana_weeklyList").append("<button type='button' class='btn btn-info btn-lg' " +
			   "onClick='Bhavana_openWeekMeditation(" + bhavana_weekly[item].code + ")'>" +
			   bhavana_weekly[item].name + "</button>" +
			   "<div class='card'>" + bhavana_weekly[item].desc + "</div>" +
			   "<hr />");
			   */
		}

		Cala.workingOn.end();

	});

}

/* Opens a player with the meditation */
function Bhavana_weekMeditationOpen(code, name){

	Cala.say("Loading week meditation: " + code + " / " + name);
	$('#bhavana_weeklyPlayer').show();
	$('#bhavana_weeklyTitle').html(name);
	var audio = $("#bhavana_player");
	$("#bhavana_playerSource").attr("src", 'https://lasangha.org/files/bhavana/meditaciones/' + code);
	audio[0].pause();
	audio[0].load();//suspends and restores all audio element
	$("html, body").animate({ scrollTop: 0 }, "slow");

	// Add the download link
	$('#bhavana_weeklyDownloadLink').attr('href', 'https://lasangha.org/files/bhavana/meditaciones/' + code);

	return true;

}

/***************************************************************************
 *
 * Weekly podcast
 *
 */

/**
 * Get the weekly meditations list
 */
function Bhavana_podcastGetList(){

	Cala.workingOn.start();

	Cala.say("Getting the script");

	var weeklyTpl = "<div class='card'><div class='card-header'><h2>{name}</h2></div>" +
		"<div class='card-body'>" +
		"<p class='card-text'>{desc}</div>" +
		"<button type='button' class='btn btn-success' onClick='Bhavana_podcastOpen(\"{code}\", \"{name}\")'>Cargar <i class='fa fa-eye' aria-hidden='true'></i></button>" +
		"</div></div><hr />";

	$.getScript(Cala.ip + "bhavana_podcast.js", function(data, textStatus, jqxhr){

		Cala.say("Got the weekly list: " + JSON.stringify(bhavana_podcast));

		for(var item in bhavana_podcast){
			Cala.say("Adding podcast to the list" + JSON.stringify(bhavana_podcast[item]));
			$("#bhavana_podcastList").append(parseTpl(weeklyTpl, bhavana_podcast[item], true));
		}

		Cala.workingOn.end();

	});

}

/* Opens a player with the meditation */
function Bhavana_podcastOpen(code, name){

	Cala.say("Loading week meditation: " + code + " / " + name);
	$('#bhavana_podcastPlayer').show();
	$('#bhavana_podcastTitle').html(name);
	var audio = $("#bhavana_player");
	$("#bhavana_playerSource").attr("src", 'https://lasangha.org/files/bhavana/podcast/' + code);
	audio[0].pause();
	audio[0].load();//suspends and restores all audio element

	// Add the download link
	$('#bhavana_podcastDownloadLink').attr('href', 'https://lasangha.org/files/bhavana/podcast/' + code);

	/*
	   frame = '<div id="cala_frameLoading" class="alert alert-info" type="alert">Cargando... <img src="modules/bhavana/img/loader_balls.gif" /></div>';

	   frame = frame + "<iframe id='audio_" + code + "' frameborder='0' allowfullscreen='' scrolling='no' height='200' style='border:1px solid #EEE; box-sizing:border-box; width:100%;' src='https://www.ivoox.com/player_ej_" + code  + "_4_1.html?c1=ff6600'></iframe>";

	   $("#bhavana_weeklyMedFrame").html(frame);

	   $('#audio_' + code).on("load", function() {
	   Cala.say("Meditation loaded, I will hide some stuff");
	   $('#cala_frameLoading').hide('slow');
	   $("#bhavana_weeklyMedFrame").slideDown('slow');
	   });

	   $(window).scrollTop(0);
	   */
	$("html, body").animate({ scrollTop: 0 }, "slow");
	return true;

}

/* Notifications */
function bhavana_notificationsDefault(){

	var notificationsDisplay = false;

	// Hide by default
	//$('#bhavana_notificationsAll').hide();

	// First time?
	if(Cala.Keys.get('bhavana_notifications_firstRun', 0) == 0){
		Cala.say('First run');
		Cala.noticesAdd({
			title: "¡Una bienvenida!",
			text: "Gracias por tomar el Retiro Personal de Meditación.\n Visite la sección de <a href='?x=bhavana/support'>Ayuda y Soporte</a> para conocer más acerca de esta herramienta y las opciones de seguimiento que tiene a su disposición.",
		}, 'bhavana_firstRun');

		/*
		   sweetAlert({
		   title: "¡Una bienvenida!",
		   text: "Gracias por tomar el Retiro Personal de Meditación.\n Visite la sección de <a href='?x=bhavana/support'>Ayuda y Soporte</a> para conocer más acerca de esta herramienta y las opciones de seguimiento que tiene a su disposición.",
		   type: "success",
		   },function(){
		//Cala.iGoTo("?x=login&src=app_notLoggedIn");
		});
		*/

	}

	// Register
	if(Cala.Keys.get("bhavana_notifications_registerDone", '0') == '0'){
		Cala.say("Not registred yet!");
		/*
		   Cala._noticesAdd($('#bhavana_notificationsRegister').html(), '_bhavana_notifications_register');
		   Cala.noticesCounterPlus();
		   */
		Cala.noticesAdd({
			title: "Regístrese",
			text: "Reciba información cada semana con nuevas meditaciones, temas de estudio y otros recursos importantes para tener el mejor provecho de este curso.",
			takeMeTo: "bhavana/registerCourse",
		}, 'bhavana_notificationsRegister');

	}else{
		Cala.say("Already registred");
	}

	Cala.loadScript(Cala.ip + 'bhavana_push.js', function(){

		// New podcast
		var idPodcast = bhavana_notificationsGetId('podcast');

		if(bhavana_notifications.podcast.id == idPodcast && Cala.Keys.get(idPodcast, 0) == 0){

			Cala.say("There is a new podcast!");

			Cala.noticesAdd({
				title: "Comprender y Despertar: " + bhavana_notifications.podcast.title,
				text: bhavana_notifications.podcast.desc, 
				takeMeTo: "bhavana/podcast",
			}, idPodcast);
		}else{
			//$('#bhavana_notificationsPodcast').hide(); 
			Cala.say("No podcast or already viewed");
		}

		/*
		if(bhavana_notifications.podcast == false || Cala.Keys.get(idPodcast, 0) == 1){
			Cala.say("No podcast or already viewed");
			//$('#bhavana_notificationsPodcast').hide(); 
		}else{
			Cala.say("There is a new podcast!" + JSON.stringify(bhavana_notifications.podcast));

			Cala.noticesAdd({
				title: "Comprender y Despertar: " + bhavana_notifications.podcast.title,
				text: bhavana_notifications.podcast.desc, 
				takeMeTo: "bhavana/podcast",
			}, idPodcast);
		}
		*/
		//$('#bhavana_notificationsPodcastTitle').html('<strong>' + bhavana_notifications.podcast + '</strong>');
		//Cala._noticesAdd($('#bhavana_notificationsPodcast').html(), '_bhavana_notifications_podcast');

		// New meditation
		var idWeekly = bhavana_notificationsGetId('weekly');
		if(bhavana_notifications.weekly.id == idWeekly && Cala.Keys.get(idWeekly, 0) == 0){
			Cala.say("There is a new meditation!");

			Cala.noticesAdd({
				title: "Meditación de la semana: " + bhavana_notifications.weekly.title,
				text: bhavana_notifications.weekly.desc, 
				takeMeTo: "bhavana/weekly",
			}, idWeekly);

			//$('#bhavana_notificationsWeeklyTitle').html('<strong>' + bhavana_notifications.weekly + '</strong>');
			//Cala._noticesAdd($('#bhavana_notificationsWeekly').html(), '_bhavana_notifications_weekly');
			//Cala.noticesCounterPlus();
			//notificationsDisplay = true;
		}else{
			Cala.say("No meditation or already viewed");
		}


		/*
		// New post
		var idPost = bhavana_notificationsGetId('post');
		if(bhavana_notifications.post == false || Cala.Keys.get(idPost, 0) == 1){
		Cala.say("No post or already viewed");
		//$('#bhavana_notificationsPost').hide();
		}else{
		Cala.say("There is a new post!");
		$('#bhavana_notificationsPostTitle').html('<strong>' + bhavana_notifications.post + '</strong>');
		notificationsDisplay = true;
		}
		// New session
		var idSession = bhavana_notificationsGetId('session');
		if(bhavana_notifications.session == false || Cala.Keys.get(idSession, 0) == 1){
		Cala.say("No session or already viewed");
		//$('#bhavana_notificationsSession').hide();
		}else{
		Cala.say("There is a new session!");
		notificationsDisplay = true;	
		}

		// Hide all notifications panel if nothing is pending
		if(notificationsDisplay == true){
		Cala.say("There are notifications, I will show them");
		//$('#bhavana_notificationsAll').slideDown('slow');
		}
		*/
		//console.log(JSON.stringify(bhavana_notifications));
	});
}

// Stop showing notifications
function bhavana_notificationsHide(which){

	Cala.say("Stoping notification about: " + which);

	//var id = bhavana_notificationsGetId(which);

	Cala.Keys.store(which, '1');

	Cala.noticesRm("_bhavana_notifications_" + which);

	return false;

}

// Get notifications ids and keys for storing
function bhavana_notificationsGetId(which){

	Cala.say("Getting id for notification: " + which);
	var w = Cala_timeGetWeek(new Date());
	var id = 'bhavana_notifications_' + which + '_w' + w[1] + '_y' + w[0];
	Cala.say("_Got: " + id);
	return id;
}

/* Clear everything up */
function bhavana_clearUp(){
	Cala.Keys.remove('var_bhavana_lastPage');
	for(var i =0; i < localStorage.length; i++){
		//console.log(localStorage.getItem(localStorage.key(i)));
		if(localStorage.key(i).indexOf('bhavana_notifications_') >= 0){
			console.log("Found value");
			console.log(localStorage.key(i));
			Cala.Keys.remove(localStorage.key(i));
		}
	}

}

