// Youtube, This is all here because I could not put it inside the Bhavana object
var bhavana_thisVideoId    = 'UD-iWHfq-hY';
var bhavana_meditationTotalTime = 0;

//2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

//tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
    Cala.say("Creating the player");
    player = new YT.Player('sessionsPlayer', {
        height: '390',
        width: '640',
        videoId: bhavana_thisVideoId,
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
    Cala.say("Player is done and ready 3");
    player.cueVideoById({'videoId': bhavana_thisVideoId, 'suggestedQuality': 'small'});
    //event.target.playVideo();
    $("#bhavana_waitingForVideo").hide('slow');
	// It is not a lecture, I will give instructions
	if(Bhavana_Session.dets.isLecture == false){
		Cala.say("This is not a lecture, I will show instructions");
		$("#bhavana_videoInstruccionsReady").show();
	}else{
		Cala.say("This is a lecture, I will not show instructions");
		$("#bhavana_videoInstruccionsReady").hide();
	}
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
function onPlayerStateChange(event) {

	Cala.say("Player state" + player.getPlayerState());

	// Is it a meditation video?
	if(Bhavana_Session.dets.isLecture == true){
		return false;
	}

	// Count only on pauses or end
	if (event.data == YT.PlayerState.ENDED || event.data == YT.PlayerState.PAUSED) {
		Cala.say("Adding time");

		// Time in the video in minutes
		currentTime = (Math.floor(player.getCurrentTime() / 60));

		// Only if this is more than 1 minute
		if(currentTime >= 1){
			totalTime = currentTime - bhavana_meditationTotalTime;
			Cala.say("Time is: " + currentTime + " last: " + bhavana_meditationTotalTime + " total: " + totalTime);

			// This should be given in minutes, I wil only do it if it is more than 1
			if(totalTime < 1){
				Cala.say("Not enought time");
				return false;
			}

			// Register the time in the system
			Bhavana_Session.registerTime(totalTime);
			// Bring time up to speed
			bhavana_meditationTotalTime = currentTime;

		}else{
			Cala.say("Less than a minute");
		}
	}

}

function stopVideo() {
	Cala.say("Video stoped");
	player.stopVideo();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

// Samatha 1 - Tranquilidad
var Bhavana_Categories = {
	samatha: {
		title: "Samatha",
		desc: "Tranquilidad, volviendo al cuerpo.",
		duration: 9,
		sessions: ['SKejPyKEugI', 'ZUXoV4mDneE', 'gVp5UJ-AlGQ',
		'MzXxa5j3aSo', 'BKKgNIXAZss', '1FFeB-zXerc',
		'3ucVT8Vcp-s', 'JQpTz9i6vKM', 'i1UsmfBfCwE',
		'eOCDcu29lLQ', 'TVBaruAy42c', 'CX_S6ODW7FM',
		'WvjRjzdVpOw'],
		lectures: ['2','5','8']
	},
	vipassana: {
		title: "Vipassana",
		desc: "Introspección y descubrimiento de la mente.",
		duration: 15,
		sessions: ['CtoUlB33gc0', '8luztS3gsCU', 'dUeFtXXan0w',
		'Stejz6ljj0U', 'g5gQpJRbpXQ', 'C44O4U5iNU4',
		'tR6lRnlqzfk', 'o2j9srT_OWo', '9Jo69GLmZH4',
		'bXzfYR-QAEk', 'ezeBJrcwyj0', 'IlwQw9pyEPA',
		'IALf1A58H-U', 'vA9yR7UPtnI', 'tfKiYSviV7w', 'UmiEBcAzLGI'],
		lectures: ['1', '4', '7', '10', '13', '16']
	},
	life: {
		title: "Vida Diaria",
		desc: "Cada momento es una oportunidad para meditar.",
		duration: 25,
		sessions: ['oB8Qhk8nIp0', 'AcgV968LLVQ', 'GBw91H9ogbo',
		'PGSzlrypAdo', 'R6rUZedEem4', 'vSdM8H4I7VY',
		'gz_9_hrRh-8', 'fmSP93pjxJg', 'RthTLblxKcY',
		'XM-IP3yk7OU', 'ATjiTbrFN-Y', 'R7l2HCoLOOw',
		'H_hC3PZkiNk', 'UFiBb1gUb6Q', 'pn96zIh0WqE'],
		lectures: ['3', '6', '9', '13', '15']
	}

};

var Bhavana_Session = {

	dets: {
		title: "noName",
		desc: "Description",
		sessions: [],
		id: 0,
		duration: 0,
		cat: false,
		isLecture: false,
		code: 'non' //For this meditation session
	},
	setSessionStuff: function(_dets){
		Cala.say("setting stuff");
		this.dets = _dets;
		return this;
	},
	getCategory: function(categories){
		// The requested category
		cat = Cala.paramsGet("cat", 'samatha');
		Cala.say("Looking for cat: " + cat);
		if(categories.hasOwnProperty(cat)){
			//Do nothing really
			Cala.say("Got category:" + cat);
			this.setSessionStuff(categories[cat]);
			this.dets.cat = cat;
		}else{
			Cala.say("Category does not exist :(");
			this.dets.cat = false;
		}
		return this;
	},
	getSessionId: function(){
		if(this.dets.cat !== false){
			this.dets.id = Cala.paramsGet("sessionId", "1");
			Cala.say("Got session id: " + this.dets.id);
			if(this.dets.id > this.dets.sessions.length){
				Cala.say("This is not a valid session");
				this.dets.id = 0;
			}
		}
		return this;
	},
	loadSession: function(){
		if(this.dets.id > 0){
			Cala.say("Setting session");

			// Is this a class?
			if(this.dets.lectures.indexOf(this.dets.id) >= 0){
				this.dets.isLecture = true;
				Cala.say("This is a lecture: " + this.dets.id + " -- " + this.dets.lectures.indexOf(this.dets.id) + " -- " + this.dets.isLecture);
				// Change the title
				title = "Clase";
				// Hide stuff
				$("#bhavana_medStuff").hide();
				this.loadVideo();	
			}else{
				this.dets.isLecture = false;
				Cala.say("This is NOT a lecture: " + this.dets.id + " -- " + this.dets.lectures.indexOf(this.dets.id) + " -- " + this.dets.isLecture);
				title = this.dets.desc;
			}

			$("#sessionTitle").html(this.dets.title);
			$("#sessionNumber").html("# " + this.dets.id);
			$("#sessionDesc").html(title);
			bhavana_thisVideoId = this.dets.sessions[this.dets.id - 1];
			Cala.say("Video code: " + bhavana_thisVideoId);
		}
		return this;
	},
	setPager: function(categories){
		// Get the possition in the array of key of this category
		sessions = Object.keys(categories);
		sessionsPos = sessions.indexOf(this.dets.cat);
		Cala.say("SessionsPos: " + sessionsPos + " sessions: " + sessions.length);

		catId = this.dets.cat + "_" + this.dets.id;
		
		// Next?
		if(this.dets.id >= this.dets.sessions.length){
			Cala.say("This is the last of this series");

			// Are there more series?
			if(sessionsPos >= (sessions.length - 1)){
				$("#goNext").attr("href", "?x=bhavana/c_vidaDiariaEnd");
			}else{
				$("#goNext").attr("href", "?x=bhavana/sessions&sessionId=1&cat="+sessions[parseInt(sessionsPos) + 1]);
			}
			$("#" + catId).attr('class', 'btn btn-success btn-block btn-lg');
		}
		else{
			Cala.say("There are more sessions to go...");
			$("#goNext").attr("href", "?x=bhavana/sessions&sessionId=" +(parseInt(this.dets.id) + 1 )+"&cat="+this.dets.cat);

			// Change the button and set focus
			//catId = this.dets.cat + "_" + this.dets.id;
			catIdNext = this.dets.cat + "_" + (parseInt(this.dets.id) + 1);
			Cala.say("Marking this: " + catId);
			$("#" + catId).attr('class', 'btn btn-success btn-block btn-lg');
			$("#" + catIdNext).html($("#" + catIdNext).html() + ' (Siguiente)');
		}

		// Back
		// There are places to go back
		if(this.dets.id > 1){
			Cala.say("There are more sessions to go back to in this category");
			$("#goBack").attr("href", "?x=bhavana/sessions&sessionId=" +(parseInt(this.dets.id) - 1 )+"&cat="+this.dets.cat);
		}
		else{
			// Is this the first of the sessions?
			if(sessionsPos === 0){
				Cala.say("This is the first of the series, going back to the main page");
				backPath = "?x=bhavana/c_intro";
			}
			// Lets find out go goes before
			else{
				Cala.say("Which is the previews category?");
				backPath = "?x=bhavana/sessions&sessionId=" + categories[this.dets.cat].sessions.length + "&cat="+sessions[parseInt(sessionsPos) -1];
			}
			$("#goBack").attr("href", backPath);
		}
	},
	hidePlayer: function(){
		$("#bhavana_videoContainer").hide();
		$("#bhavana_videoInstruccionsReady").hide();
		return this;
	},
	registerTime: function(time){
		Cala.say("Registering times");

		// New system
		Bhavana_addToCause(time, this.dets.code, bhavana_thisVideoId);

		// Old system
		/*
		   Cala.connectToMainServer(
		   function(){
		   Bhavana_addToCause(time, this.dets.code, bhavana_thisVideoId);
		   });
		   */
	},
	// This actually loads the video
	loadVideo: function(){
		Cala.say("Loading the video");
		$("#bhavana_videoContainer").fadeIn('slow', function(){
			$("#bhavana_meditationInstructions").hide('slow');
			$("#bhavana_waitingForVideo").show('slow');
			Cala.say("Running youtube api");
			tag.src = "https://www.youtube.com/iframe_api";	
		});

		return this;
	},

	// Starts the meditation, it actually loads the video and registers the code
	startMeditation: function(code){

		Cala.say("Creating the player and loading the video");

		// Store the code
		this.dets.code = code;

		// Hide and show things
		$("#bhavana_selectMeditationIntention").fadeOut('slow', function(){
			Cala.say("About to load the video");
			Bhavana_Session.loadVideo();	
		});
		return false;
	},
	boot: function(categories){
		this.hidePlayer().getCategory(categories).getSessionId().loadSession().setPager(categories);
	}
};

Bhavana_Session.boot(Bhavana_Categories);
Bhavana_storeThisPage();

// @todo Use the correct way for this
// I'm I logged in?
Cala.runOnReady(function (){
	/*
	   if (Drupal.user.uid === 0) {
	   Cala.messagesClear().info("Ingrese si desea que estas meditaciones queden grabadas en su historial.");
	   }else{
	   Cala.say("User is logged in, this session will be registred in his/her personal history");
	   }
	   */
});


/*   GONE */
function bhavana_loadSession(publicName, level, session){

	Cala.say("Loading the meditation ");

	// Hide stuff
	$("#bhavana_selectMeditationIntention").fadeIn('slow', function(){
		//$("#bhavana_meditationIntention").hide();
		$("#bhavana_medVideoDiv").fadeOut('slow', function(){
			Cala.say("all gone");
		});
	});


	Cala.say("Setting session: " + level + " - " + session);

	bhavana_meditationCurrentSession = [publicName, session, level];

	// Set the session id
	bhavana_thisVideoId = Bhavana_Categories[level].sessions[session];

	Cala.say("Video Id: " + bhavana_thisVideoId);

	// Display the player
	//$("#bhavana_meditationPlayer").show('slow');

	$("#bhavana_sessionTitle").html(publicName);
	$("#bhavana_sessionNumber").html("# " + session);

	// Set focus on top of the page
	$('html, body').animate({
		scrollTop: $("body").offset().top
	}, 1000);

	/*
	// This will react when the video ends
	$("#meditationAudio").bind("ended", function() {
	Cala.say("Video ended!!!!!!!!!!!!!!");
	$('#bhavana_modalPlayer').modal('hide');
	bhavana_timerPaintSome(100, 100);
	clearInterval(bhavana_medTimer);
	bhavana_playPausePaint('pause');
// Store this session as finished
Cala.Keys.store('bhavana_lastMeditation', bhavana_meditationCurrentSession);
Bhavana_addToCause(Math.round((10 - (bhavana_meditationLastRegTime/60))), bhavana_meditationIntention, JSON.stringify(bhavana_meditationCurrentSession));
});

// React when the modal is closed
$('#bhavana_modalPlayer').on('hidden.bs.modal', function (e) {
Cala.say("Modal player was hidden");
bhavana_timerPaintSome(10, 0);
bhavana_playPausePaint('pause');
bhavana_medVideo.pause();
});
*/
}

// This actually starts the meditation video
// @Deprecated?
function bhavana_startMeditationVideo(code, name){


	Cala.say("Starting the meditation video" + bhavana_thisVideoId);

	// Set the intention
	//$("#bhavana_meditationIntention").html(name);

	$("#bhavana_selectMeditationIntention").fadeOut('slow', function(){
		//$("#bhavana_meditationIntention").show('slow');
		$("#bhavana_medVideoDiv").fadeIn('slow', function(){
			Cala.say("Running youtube api");
			tag.src = "https://www.youtube.com/iframe_api";
		});
	});

	return false;
}

// Sugest to register if this has not happened yet

