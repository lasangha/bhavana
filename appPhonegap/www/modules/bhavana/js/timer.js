// Selects all text in a text area or input field
// Thanks to: http://javascript-array.com/scripts/onclick_select_all_text_in_field/
function bhavana_selectAll(id){
	document.getElementById(id).focus();
	document.getElementById(id).select();
}

// Sets the time for the timer
function bhavana_timerSetTime(time){
	$("#bhavana_timerTime").val(time);
	Cala.info("Tiempo a meditar: " + time, 3);
	return false;
}

// Total time to meditate
var bhavana_totalTimeInTimer = 0;
// The internal timer time
var bhavana_internalTimer = 0;

// Intention
var bhavana_intention = 'non';

// The total amount of time passed in the counter
var bhavana_totalCounterTime = 0;

// The total time that I will run for
var bhavana_totalTimeToRun = 0;

// Should I be running or not?
var bhavana_timerActive = false;

// Set a new intention if the person wants to
function bhavana_timerSetIntention(intention, fullWord){
	bhavana_intention = intention;
	$("#bhavana_myIntention").html(fullWord);
	Cala.info("Nueva intención: " + fullWord, 3);
	return false;
}

// Plays the sound
function bhavana_playSound(){
	// @todo: check for android or others
	if(Cala.onApp === true){
		Cala.say("Playing in app" + Cala_createAudioPath());
		var my_media = new Media(Cala_createAudioPath() + 'modules/bhavana/bell_1.mp3',
				// success callback
				function () { Cala.say("playAudio():Audio Success"); },
				// error callback
				function (err) { Cala.say("playAudio():Audio Error: " + JSON.stringify(err)); }
				);
		// Play audio
		my_media.play();
	}else{
		Cala.say("Playing sound..." + Cala_createAudioPath() + 'modules/bhavana/bell_1.mp3');
		var audio = new Audio(Cala_createAudioPath() + 'modules/bhavana/bell_1.mp3');
		audio.play();
	}
}

// I see if this session was long enough to be stored in the records
function bhavana_shouldICountTheSession(){

	var bhavana_newMeditatedTime = Math.floor(bhavana_totalCounterTime/60);
	// @debuging
	//bhavana_newMeditatedTime = bhavana_totalCounterTime;

	Cala.say("Total Meditated time: " + bhavana_newMeditatedTime + " out of " + bhavana_totalTimeToRun);

	if(bhavana_newMeditatedTime > 0){

		Cala.say("I might be counted");

		newTime = Bhavana_timeRegister(bhavana_newMeditatedTime);

		if(newTime !== false){
			// Store the time
			Cala.say("Got totaltime: " + newTime);
			Bhavana_addToCause(newTime, bhavana_intention, 'timer');
			/*
			Cala.connectToMainServer(
					function(){
						//Bhavana_addToCause(time, this.dets.code, bhavana_thisVideoId);
						Bhavana_addToCause(newTime, bhavana_intention, 'timer');
					});
					*/
		}
	}

}

// I timing function
function bhavana_timer(){

	Cala.say("Total seconds: " + bhavana_totalCounterTime);
	if(bhavana_timerActive){
		Cala.say("Timer is running, I will set another round");
		bhavana_totalCounterTime = bhavana_totalCounterTime + 1;
		bhavana_timerPaintSome();
		setTimeout(function(){bhavana_timer()}, 1000);
	}else{
		Cala.say("Timer is stoped");
	}

}

// Add some color to the timing process
function bhavana_timerPaintSome(){

	valueNow = Math.round((bhavana_totalCounterTime/bhavana_totalTimeToRun)*100);

	Cala.say("Adding some colour to the timer: " + valueNow);

	$("#bhavana_timerProgressBar").css('width', valueNow+"%");
	$("#bhavana_timerProgressBar").attr('aria-valuenow', valueNow);

	// I can stop the timer too
	if(valueNow == 100){
		bhavana_timerFullStop(true);
	}

}

// Do a full stop of the timer
function bhavana_timerFullStop(complete){

	// Let the timer now it should stop
	bhavana_timerActive = false;

	if(complete === true){
		Cala.say("This was a complete run, I will ring the bell!");
		$("#bhavana_timerStopButton").hide('slow', function(){
			//$("#bhavana_timerStartButton").show('slow');
			$("#allVisibleStuff").show('slow');
		});
		bhavana_playSound();
	}

	// Should I count the time?
	bhavana_shouldICountTheSession();
}

// Start and stop the timer
function bhavana_timerStartStop(){

	// Clean up the last time counted
	bhavana_meditationTotalTime = 0;

	bhavana_totalTimeToRun = parseInt($("#bhavana_timerTime").val()) * 60;

	Cala.say("I will meditate for: " + bhavana_totalTimeToRun);

	if(bhavana_totalTimeToRun === 0 || isNaN(bhavana_totalTimeToRun)){
		Cala.danger("¿Cuánto tiempo va a meditar?", 3);
		$("#bhavana_timerTime").focus();
		return false;
	}

	if(bhavana_timerActive === false){
		Cala.say("Starting the timer");

		// Keep the screen on
		if(Cala.onApp === true){
			Cala.say("Screen must be on");
			keepscreenon.enable();
			Cala.say("Screen must be on?");
		}

		bhavana_timerActive = true;
		$("#allVisibleStuff").hide('slow', function(){
			$("#bhavana_timerStopButton").show('slow');
		});
		bhavana_totalCounterTime = 0;
	}else{
		Cala.say("Stoping the timer");
		bhavana_timerActive = false;
		$("#bhavana_timerStopButton").hide('slow', function(){
			//$("#bhavana_timerStartButton").show('slow');
			$("#allVisibleStuff").show('slow');
		});
		// Should I count this session?
		bhavana_timerFullStop(false);
	}
	bhavana_timer();
}

// Set some default stuff
$("#bhavana_timerStopButton").hide();
$("#bhavana_timerTime").focus();

// Keep the screen on
// @todo set this on only if the timer is running
//Cala_keepScreenOn();
