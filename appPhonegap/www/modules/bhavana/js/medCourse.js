/****************************************************************************
/*!
 * Meditation Course (v2 or more actually)
 * This file should handle the entire meditation course
 * @requires Cala Framework LATEST! always
 *
 * Copyright (c) 2016 Twisted Head
 * License: MIT
 *
 * version: 0.1.x
 *****************************************************************************/
// Audios path
var bhavana_audiosPath = 'https://files.lasangha.org/meditation/audios/';
var bhavana_audiosPath = 'http://localhost/tests/audio/';
var bhavana_audiosPath = 'http://localhost/t/meditationAudios/';

// Meditation intention
var bhavana_meditationIntention = 'non';

// This session information
var bhavana_meditationCurrentSession = [];

// Last session information
var bhavana_meditationLastSession = [];

var bhavana_medVideo = null; //document.getElementById("meditationAudio"); 

var bhavana_medTimer = false;//setInterval(bhavana_timeMeditation, 4000);

// The last registered time, for each pause in the session
var bhavana_meditationLastRegTime = 0;

// Time the meditation
function bhavana_timeMeditation(){
    if(bhavana_medVideo.paused){
        Cala.say("The video is paused, I will not get the current time");
        return true;
    }else{
        Cala.say("Getting current meditation time");
        bhavana_getVideoTime();        
    }
}

// Change the image in the play/pause button
function bhavana_playPausePaint(task){
    if(task == 'play'){
        $("#bhavana_playPauseMed").html('<i class="fa fa-pause-circle-o fa-5x"></i>');
        $("#bhavana_timerProgressBar").addClass("active");
    }else{
        $("#bhavana_playPauseMed").html('<i class="fa fa-play-circle fa-5x"></i>');
        $("#bhavana_timerProgressBar").removeClass("active");  
    }
}

// Play/pause the meditation
function bhavana_playPause(){
    if(bhavana_medVideo.paused){
        bhavana_checkMeditationIntention();
        bhavana_medVideo.play();
        bhavana_playPausePaint('play');
        bhavana_medTimer = setInterval(bhavana_timeMeditation, 4000);
    }
    else{
        // Should i count it?
        bhavana_meditationRegister();
        bhavana_medVideo.pause();
        clearInterval(bhavana_medTimer);
        bhavana_playPausePaint('pause');
    }
}

// Should I paint the progress bar?
function bhavana_getVideoTime(){
    curTime  = bhavana_medVideo.currentTime;
    duration = bhavana_medVideo.duration;
    console.log("Elapsed time: " + curTime + " / out of: " + duration);
    bhavana_timerPaintSome(duration, curTime);
}

// I decide if the meditated time should be counted
function bhavana_meditationRegister(){

    Cala.say("Should I register this time?");

    // Get the current time
    newTime = (Math.floor((bhavana_medVideo.currentTime - bhavana_meditationLastRegTime) / 60));

    Cala.say("Time is: " + bhavana_medVideo.currentTime + " last: " + bhavana_meditationLastRegTime + " new: " + newTime);

        // Only if this is more than 1 minute
        if(newTime >= 1){
            // Register the time in the system
            Bhavana_addToCause(newTime, bhavana_meditationIntention, JSON.stringify(bhavana_meditationCurrentSession));
            // Bring time up to speed
            bhavana_meditationLastRegTime = bhavana_medVideo.currentTime;
        }else{
            Cala.say("Less than a minute");
        }
}

// Add some color to the timing process
function bhavana_timerPaintSome(totalTime, elapsedTime){

    valueNow = Math.round((elapsedTime/totalTime)*100);

    Cala.say("Adding some colour to the timer: " + valueNow);

    $("#bhavana_timerProgressBar").css('width', valueNow+"%");
    $("#bhavana_timerProgressBar").attr('aria-valuenow', valueNow);

    // Should I end this thing?
    if(valueNow == 100){
        Cala.say("The meditation has ended...");
        //bhavana_playPause();
        sweetAlert({title: "Felicidades!",
                text: "Ha terminado la sesión. <br /> Recuerde estar bien siempre :)",
                type: "success",
                html: true,
                },function(){
                Cala.iGoTo("?x=bhavana/medCourse");
                });
    }

}
var bhavana_course = {

    // The current course level
    currentLevel: 0,
    // The current sub level (up to 10)
    currentSubLevel: 0,
    lastSession: 0,

    // All meditation course levels
    levels: {
        basics: {
            title: "Las bases",
            desc: "Bases de la meditación.",
            sessions: 10,
        },
        samatha: {
            title: "Samatha",
            desc: "Estableciendo una concentración apropiada.",
            sessions: 10,
        },
        vipassana: {
            title: "Vipassana",
            desc: "Abriendo la mente a una nueva forma de ver las cosas.",
            sessions: 10, 
        },
        life_0: {
            title: "En las vida diaria 1",
            desc: "Utilizando lo que hemos aprendido en la vida diaria.",
            sessions: 10,
        }
    },
    sessionDets: {
        title: "Sesión sin nombre",
        desc: "Descripción",
        sessions: [],
        id: 0,
        duration: 0,
        cat: false,
        code: 'non' //For this meditation session
    },
    setSessionStuff: function(_dets){
        Cala.say("Setting stuff for a meditation session.");
        this.dets = _dets;
        return this;
    },
    getCategory: function(levels){
        // The requested category
        cat = Cala.paramsGet("cat", 'samatha');
        Cala.say("Looking for cat: " + cat);
        if(levels.hasOwnProperty(cat)){
            //Do nothing really
            Cala.say("Got category:" + cat);
            this.setSessionStuff(levels[cat]);
            this.sessionDets.cat = cat;
        }else{
            Cala.say("Category does not exist :(");
            this.sessionDets.cat = false;
        }
        return this;
    },
    getSessionId: function(){
        if(this.sessionDets.cat !== false){
            this.sessionDets.id = Cala.paramsGet("sessionId", "1");
            Cala.say("Got session id: " + this.sessionDets.id);
            if(this.sessionDets.id > this.sessionDets.sessions){
                Cala.say("This is not a valid session");
                this.sessionDets.id = 0;
            }
        }
        return this;
    },
    loadSession: function(){
        if(this.sessionDets.id > 0){
            Cala.say("Setting session: " + this.sessionDets.id);
            $("#sessionTitle").html(this.sessionDets.title);
            $("#sessionNumber").html("# " + this.sessionDets.id);
            $("#sessionDesc").html(this.sessionDets.desc);
            //@todo set this right
            this.videoId = this.sessionDets.sessions[this.sessionDets.id - 1];
        }
        return this;
    },
    setPager: function(categories){
        // Get the possition in the array of key of this category
        sessions = Object.keys(categories);
        sessionsPos = sessions.indexOf(this.dets.cat);
        Cala.say("SessionsPos: " + sessionsPos + " sessions: " + sessions.length);

        // Next?
        if(this.dets.id >= this.dets.sessions.length){
            Cala.say("This is the last of this series");

            // Are there more series?
            if(sessionsPos >= (sessions.length - 1)){
                $("#goNext").attr("href", "?x=bhavana/c_vidaDiariaEnd");
            }else{
                $("#goNext").attr("href", "?x=bhavana/sessions&sessionId=1&cat="+sessions[parseInt(sessionsPos) + 1]);
            }
        }
        else{
            Cala.say("There are more sessions to go...");
            $("#goNext").attr("href", "?x=bhavana/sessions&sessionId=" +(parseInt(this.dets.id) + 1 )+"&cat="+this.dets.cat);
        }

        // Back
        // There are places to go back
        if(this.dets.id > 1){
            Cala.say("There are more sessions to go back to in this category");
            $("#goBack").attr("href", "?x=bhavana/sessions&sessionId=" +(parseInt(this.dets.id) - 1 )+"&cat="+this.dets.cat);
        }
        else{
            // Is this the first of the sessions?
            if(sessionsPos === 0){
                Cala.say("This is the first of the series, going back to the main page");
                backPath = "?x=index";
            }
            // Lets find out go goes before
            else{
                Cala.say("Which is the previews category?");
                backPath = "?x=bhavana/sessions&sessionId=" + categories[this.dets.cat].sessions.length + "&cat="+sessions[parseInt(sessionsPos) -1];
            }
            $("#goBack").attr("href", backPath);
        }
    },
    hidePlayer: function(){
        //$("#sessionsPlayer").hide();
        //$("#bhavana_selectMeditationIntention").hide();
        return this;
    },
    registerTime: function(time){
        Cala.say("Registering times");
        Bhavana_addToCause(time, this.dets.code, bhavana_thisVideoId);
    },
    // Starts the meditation, it actually loads the video and registers the code
    startMeditation: function(code){

        Cala.say("Creating the player and loading the video");

        // Store the code
        this.dets.code = code;

        // Hide and show things
        $("#bhavana_selectMeditationIntention").fadeOut('slow', function(){
            $("#bhavana_meditationInstructions").fadeIn('slow', function(){
                $("#bhavana_waitingForVideo").show('slow');
                Cala.say("Running youtube api");
                tag.src = "https://www.youtube.com/iframe_api";
            });
        });
        return false;
    },

    boot: function(categories){
        this.hidePlayer().getCategory(categories).getSessionId().loadSession().setPager(categories);
    }

};

// Loads the session in the player
function bhavana_loadSession(publicName, level, session){

    Cala.say("Setting session: " + level + " - " + session);

    bhavana_meditationCurrentSession = [publicName, session, level];

    // Clean up the timer just in case
    bhavana_timerPaintSome(10, 0);

    // Display the player
    //$("#bhavana_meditationPlayer").show('slow');

    $("#bhavana_sessionTitle").html(publicName);
    $("#bhavana_sessionNumber").html("# " + session);

    {
        /*
        $("#bhavana_medVideoPlayer").html('<video width="320" height="240" id="meditationAudio" >'+
                                          '<source src="' + bhavana_audiosPath + 
                                              "?level=" + level + '&session=' + session +
                                                  '" type="video/mp4"></source>' + '</video>');
                                                 */
        $("#bhavana_medVideoPlayer").html('<video width="320" height="240" id="meditationAudio" >'+
                                          '<source src="' + bhavana_audiosPath + 
                                              level + "/" + level + '_' + session + '.mp3' +
                                                  '" type="video/mp4"></source>' + '</video>');

    }

    // Load this here once the audio/video tag has been (re)created
    bhavana_medVideo = document.getElementById("meditationAudio");

    // Set focus on top of the page
    $('html, body').animate({
        scrollTop: $("body").offset().top
    }, 1000);

    // This will react when the video ends
    $("#meditationAudio").bind("ended", function() {
        Cala.say("Video ended!!!!!!!!!!!!!!");
        $('#bhavana_modalPlayer').modal('hide');
        bhavana_timerPaintSome(100, 100);
        clearInterval(bhavana_medTimer);
        bhavana_playPausePaint('pause');
        // Store this session as finished
        Cala.Keys.store('bhavana_lastMeditation', bhavana_meditationCurrentSession);
        Bhavana_addToCause(Math.round((10 - (bhavana_meditationLastRegTime/60))), bhavana_meditationIntention, JSON.stringify(bhavana_meditationCurrentSession));
    });

    // React when the modal is closed
    $('#bhavana_modalPlayer').on('hidden.bs.modal', function (e) {
        Cala.say("Modal player was hidden");
        bhavana_timerPaintSome(10, 0);
        bhavana_playPausePaint('pause');
        bhavana_medVideo.pause();
    });
}

// Check if there is a meditation intention, this is only if the person does not select anything
function bhavana_checkMeditationIntention(){
    $("#bhavana_meditationIntention").show('slow');
    $("#bhavana_selectMeditationIntention").hide('hide');
}

// Set the meditation intention
function bhavana_setMeditationIntention(intention, text){
    Cala.say("Setting the meditation: " + text);
    $("#bhavana_meditationIntention").show('slow');
    $("#bhavana_meditationIntention").html(text);
    $("#bhavana_selectMeditationIntention").hide('hide');
    bhavana_meditationIntention = intention;
    return false;
}

// Creates a session list for the person to choose from
function bhavana_createSessionsList(publicName, level){

    sessionList = "";

    for(i = 1; i < 11; i++){
        sessionList = sessionList + '<button class="btn btn-success btn-block btn-lg btn-round-lg"' +
            'data-toggle="modal" type="button" data-target="#bhavana_modalPlayer"' +
                'onClick="bhavana_loadSession(\'' + publicName + '\', \'' + 
                    level + '\', ' + i + ')" '+
                        'id="bhavana_sessionMed_' + level + '_' + i + '">' + i + '</button><br />';
    }

    return sessionList;
}

$('#bhavana_sessions_basics').html(bhavana_createSessionsList('Lo básico', 'foundations'));
$('#bhavana_sessions_samatha').html(bhavana_createSessionsList('Concentración', 'samatha'));
$('#bhavana_sessions_vipassana').html(bhavana_createSessionsList('Introspección', 'vipassana'));
$('#bhavana_sessions_life_0').html(bhavana_createSessionsList('En la Vida Diaria', 'life_0'));

// Set the information about the last meditation session
function bhavana_setLastMeditationSessionInfo(){

    Cala.say("Setting the information about the last meditation");

    lastMeditation = Cala.Keys.get('bhavana_lastMeditation', 'none').split(",");
    if(lastMeditation == 'none'){
        Cala.say('No last meditation information found');
        $('#bhavana_lastMeditationInfo').html("<div align='center'><h1>Esta es su primer meditación.<br />¡Inicie ahora!</h1></div>");
    }else{
        $('#bhavana_lastMeditationInfoDet').html(lastMeditation[0] + ' - ' + lastMeditation[1]);
        // Set the focus there
        focusOn = lastMeditation[2] + "_" + lastMeditation[1];
        Cala.say("Setting focus on: " + focusOn + "#bhavana_sessions"+lastMeditation[2]);
        // Open the thing
        $("#bhavana_sessions_"+lastMeditation[2]).toggle();
        $("#bhavana_sessionMed_"+focusOn).focus();
        console.log("#bhavana_sessionMed_"+lastMeditation[2] + "_" + lastMeditation[1]);
        $("#bhavana_sessionMed_"+lastMeditation[2] + "_" + lastMeditation[1]).removeClass('btn-success').addClass('btn-warning');
        //$("#bhavana_sessionMed_samatha_1").removeClass("btn-success").addClass("btn-warning");
    }

}

//Set information for the last meditation session
bhavana_setLastMeditationSessionInfo();

