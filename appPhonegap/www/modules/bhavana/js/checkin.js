// Get data of checked in mind states
function bhavana_checkinGetData(){

    // Get the data
    Drupal.services.call({
        method: 'POST',
    path: 'bhavana_resources/checkin_get_data.json',
    data: JSON.stringify(
        {period: 7}),
    success: function(result) {

        if(result == 'false'){
            Cala.info("No hay datos en este momento. Haga un checkeo mental para empezar.", 5);
            // Change the values for each case
            $("#checkinView").hide();
            //Cala_chartsNoInfo("checkInFeelingPerDayDiv", "");
            return false;
        }

        Cala.say("Result: " + JSON.stringify(result));

        // Main emotions during this period
        var mainEmotions = Object.keys(result.emotions);
        for(var emotion in result.emotions){

            Cala.say("Emotion: " + JSON.stringify(result.emotions[emotion]));

            var bgE = "";

            // Clasify the state
            if(result.emotions[emotion].type == 'p'){
                bgE += "emotionsBgPos";
            }else{
                bgE += "emotionsBgNeg";
            }

            //$("#checkInMindStates").append("<li class='list-group-item " + bg + "'>" + state + "</li>");
            $("#checkInMainInWeekEmotions").append("<li class='list-group-item " + bgE + "'>" + emotion + "</li>");

        }

        // Latest messages
        for(var message in result.latestMessages){
            Cala.say("Message: " + message);
            $("#checkInMainLatestMessages").append("<li class='list-group-item'>" + result.latestMessages[message] + "</li>");
        }

        // Mind states
        for(var pos in result.mindStates){
            Cala.say("Mind state: " + pos);

            var state = "";
            var bg = "";

            // Clasify the state
            if(result.mindStates[pos] < 3){
                state = "Muy mal";
                bg += "mindStatesBg1";
            }else if(result.mindStates[pos] < 5){
                bg += "mindStatesBg2";
                state = "Mal";
            }else if(result.mindStates[pos] < 7){
                bg += "mindStatesBg3";
                state = "Neutro";
            }else if(result.mindStates[pos] < 9){
                bg += "mindStatesBg4";
                state = "Bien";
            }else{
                bg += "mindStatesBg5";
                state = "Muy Bien";
            }

            $("#checkInMindStates").append("<li class='list-group-item " + bg + "'>(" + pos + ") " + state + "</li>");
        }

        /*
        // Total for the period
        // Get the keys
        var mainEmotions = Object.keys(result.totalsForWeek);

        var data = [
        {
        value: result.totalsForWeek[mainEmotions[0]],
        color:"#F7464A",
        highlight: "#FF5A5E",
        label: mainEmotions[0]
        },
        {
        value: result.totalsForWeek[mainEmotions[1]],
        color: "#46BFBD",
        highlight: "#5AD3D1",
        label: mainEmotions[1]
        },
        {
        value: result.totalsForWeek[mainEmotions[2]],
        color: "#FDB45C",
        highlight: "#FFC870",
        label: mainEmotions[2]
        }
        ];

        var optionsInWeek = {
        crossText: ["Felicidad! " + result.totalsForWeek.happiness + "%"],
        crossTextOverlay:   [true],
        crossTextFontSize: [34],
        crossTextFontColor: ["black"],
        crossTextRelativePosX: [2],
        crossTextRelativePosY: [2],
        crossTextAlign: ["center"],
        crossTextBaseline: ["middle"],
        inGraphDataShow: false,
        legend: true,
        legendFontSize: 18,
        legendFontColor: 'black',
        canvasBorders: false,
        graphTitleFontFamily: "'Arial'",
        graphTitleFontSize: 24,
        graphTitleFontStyle: "bold",
        graphTitleFontColor: "#000",
        responsive: true,
        dynamicDisplay: true
        };
        var checkInMainInWeekPieChart = new Chart(document.getElementById("checkInMainInWeek").getContext("2d")).Doughnut(data,optionsInWeek);
        */

        Cala.say("And now, for each day in a nice lines graph");

        // For each day
        var dataFeelingPerDay = {
            labels : result.totalsForEachDay.labels,
            datasets : [
            {
                fillColor: "rgba(70, 191, 189, 0.6)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                data: result.totalsForEachDay.data
                //title: "Para este día"
            }
            ]
        };

        var optionsFeelingPerDay = {
            inGraphDataShow : true,
            inGraphDataTmpl : "",
            datasetFill : true,
            scaleShowLabels: false,
            scaleOverride: true,
            scaleSteps : 10,
            scaleStepWidth : 1,
            scaleStartValue : 0,
            graphTitle : "Así me he sentido",
            graphTitleFontFamily : "'Arial'",
            graphTitleFontSize : 24,
            graphTitleFontStyle : "bold",
            graphTitleFontColor : "#000",
            annotateDisplay : true, 
            spaceTop : 0,
            spaceBottom : 0,
            spaceLeft : 0,
            spaceRight : 0,
            logarithmic: false,
            rotateLabels : "smart",
            startAngle : 0,
            dynamicDisplay : true,
            responsive: true,
            labelsFontColor: 'black',
            xAxisFontColor: 'black'
        };

        var checkInFeelingPerDay = new Chart(document.getElementById("checkInFeelingPerDay").getContext("2d")).Line(dataFeelingPerDay,optionsFeelingPerDay);

    },
    error: function(){
        $("#checkinView").hide();
        Cala.say("Something wrong?");
    }
    });

}

/**
 * Registers a new check in
 */
function bhavana_checkinRegister(){

    Cala.workingOn.start();
    // Get the emotion
    //var emotion = $("input[name='emotions']:checked").val();
    var emotion = $("#selectedEmotionInput").val();
    var mindState = $("#selectedMindStateInput").val();
    var message = $("#checkInMessage").val();

    // Is there something selectecd
    if(emotion === undefined || mindState === undefined){
        Cala.say("No emotion or mind state selected");
        Cala.danger("Falta información de registro", 5);
        return false;
    }

    Cala.say("The emotion is: " + emotion + " and the mind state is: " + mindState);

    // List of emotions
    var emotionsListPos = ['eno','fru','imp','dep','tri','ins','ans','pre'];
    var emotionsListNeg = ['gen','amo','com','emp','fel','agr','sat','esp'];

    if(emotionsListPos.indexOf(emotion) === -1){
        type = 'p';
        Cala.say("Type: pos");
    }else{
        Cala.say("Type: neg");
        type = 'n';
    }

    // Send the information
    Drupal.services.call({
        method: 'POST',
        path: 'bhavana_resources/checkin_register.json',
        data: JSON.stringify(
            {emotionCode: emotion,
                mindState: mindState,
        message: message,
        where: (Cala.onApp === true ? 'app' : 'web'),
        emotionType: type}),
        success: function(result) {
            Cala.say("Result: " + result);
            //Cala.success("Todo listo :)", 5);
            Cala.workingOn.end();
            Cala.iGoTo('?x=bhavana/checkInView');
        },
        error: function(){
            Cala.workingOn.end();
            Cala.say("Something wrong?");
        }
    });

}

function bhavana_checkinRegister2(emotion){
    // Get the emotion
    //var emotion = $("input[name='emotions']:checked").val();
    //var emotion = $("#selectedEmotionInput").val();

    // Is there something selectecd
    if(emotion === undefined){
        Cala.say("No emotion selected");
        Cala.danger("¿Cuál emoción desea registrar?.", 5);
        return false;
    }

    Cala.say("The emotion is: " + $("input[name='emotions']:checked").val());

    var emotionsListPos = ['eno','fru','imp','dep','tri','ins','ans','pre'];
    var emotionsListNeg = ['gen','amo','com','emp','fel','agr','sat','esp'];

    if(emotionsListPos.indexOf(emotion) === -1){
        type = 'p';
        Cala.say("Type: pos");
    }else{
        Cala.say("Type: neg");
        type = 'n';
    }


    // Send the information
    Drupal.services.call({
        method: 'POST',
        path: 'bhavana_resources/checkin_register.json',
        data: JSON.stringify(
            {emotionCode: emotion,
                where: (Cala.onApp === true ? 'app' : 'web'),
        emotionType: type}),
        success: function(result) {
            Cala.say("Result: " + result);
            Cala.success("Todo listo :)", 5);
            Cala.iGoTo('?x=bhavana/checkIn');
        },
        error: function(){
            Cala.say("Something wrong?");
        }
    });

}

