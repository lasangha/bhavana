
function Bhavana_badgesGetAll(){

    activeBadgeTpl = '<li class="list-group-item list-group-item-success">'+
        '<h3><i class="fa fa-ok" aria-hidden="true"></i> Lecturas '+
        '{title}</h3>'+
        '</li>';
    inactiveBadgeTpl = '<li class="list-group-item">'+
        '<h3><i class="fa fa-minus" aria-hidden="true"></i> Lecturas '+
        '{title}</h3>'+
        '</li>';

    Drupal.services.call({
        method: 'POST',
        path: 'bhavana_resources/badges_parse.json',
        data: JSON.stringify({which: 'meditation'}),
        success: function(result) {
            Cala.say("Got a response");
            for (var key in result) {
                Cala.say("Parsing badge: " + key);
                if(result[key].status == 1){
                    Cala.say("This is an active badge");
                    $("#bhavana_activeBadges_"+result[key].period).append(parseTpl(activeBadgeTpl, result[key], false));
                }else{
                    Cala.say("This is NOT an active badge");
                    $("#bhavana_inactiveBadges_"+result[key].period).append(parseTpl(inactiveBadgeTpl, result[key], false));
                }
            }
        }
    });
}
