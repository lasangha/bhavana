// Set the forms
$(function(){$('[data-toggle="tooltip"]').tooltip();})

//Variables

// Path for the main server USE TRAILING SLASH!!!
var bhavana_serverPath = "http://localhost/v3s/";

// Get functions
function bhavana_get(what, options){
	$.ajax({
		url: bhavana_serverPath + "bhavana/" + what,
		context: document.body
		}).done(console.log('toheu'));
}

function bhavana_FBLogin(){

	Cala.say("Logging into fb");
	openFB.login(
			function(response) {
				if(response.status === 'connected'){
					Cala.say('Facebook login succeeded, got access token: ' + response.authResponse.accessToken);
					openFB.api({
						path: '/me?fields=email,name,id',
						success: function(response) {
							Cala.say(JSON.stringify(response));
							Cala.login_fb(response.email, response.name, response.id);
							//data.name
							//document.getElementById("userPic").src = 'http://graph.facebook.com/' + data.id + '/picture?type=small';
						},
						error: function(){console.log("Error getting information about the user")}
					});
				}else{
					alert('Facebook login failed: ' + response.error);
				}
			}, {scope: 'email'});

}

// I should log you out of fb and then from the platform
function bhavana_fbLogout(){
	openFB.logout(
			function() {
				Cala.say('Logout successful from fb');
				Cala.logMeOut();
			},
			bhavana_errorHandler);
}

// This is just for tests, does nothing really
function bhavana_fbGetDets(){
	//@deprecated
	FB.api('/me?fields=id,name,email,permissions', function(response){
		Cala.say(JSON.stringify(response));
	});
}

// Special function to handle errors from the openFB thing
function bhavana_errorHandler(error) {
	console.log(error.message);
}

// Starter function to log someone in
function bhavana_login(){

	Cala.say("Loging in");

	userName = $("#myUserName").val();
	password = $("#myPwd").val();
	if(userName === "" || password  === ""){
		Cala.say("Something is missing");
		Cala.messagesClear().warning("Faltan algunos campos");
		$('#loginButton').button('reset');
		return false;
	}

	return _bhavana_logIn(userName, password, {});
	return false;

}

// Helper function to actually log someone in
function _bhavana_logIn(userName, userPassword, options){

	Cala.say("Helper login");

	Cala.workingOn.start("body");

	bhavana_get('login/' + userName + '/' + userPassword, {
		success:function(result){
			sweetAlert({title: "Listo!", text: "Hola de nuevo " + result.user.name, type: "success", timer: 2000},function(){
				//Cala.logMeInSuccess(result);
				Cala.iGoTo("index.html?src=app_login");
			});
			if(options.onSuccess){
				error();
			}
		},
		error: function(xhr, status, message){
			Cala.messagesClear().warning("Datos incorrectos, ¿Quizá olvidó su clave?");
			Cala.say("Unable to login");
			Cala.workingOn.end();
			if(options.onError){
				error();
			}
		}
	});

	return false;
}

// Check if user is logged in
function bhavana_isLoggedIn(){

	console.log("Am I logged in?");

	bhavana_get('loginyes', {});

}
