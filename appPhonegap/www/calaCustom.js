/**
 * Custom functions for your app
 */

var Custom = Custom || {};

var Custom = {

    _menuParse: function(){
        // My custom menu parse goes here
        $("#logMeIn").text("");
        $("#topMenuButton").html(Cala.getUserName());

        newLink =  '<li class="divider"></li>';
        newLink += '<a href="?x=myAccount" class="list-group-item">';
        newLink +=    '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>';
        newLink += ' Configuración</a>';
        newLink += '<a href="?x=login&logout=yes" class="list-group-item">';
        newLink +=   '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>';
        newLink += ' Salir</a>';

        $("#userMenu").append(newLink);

        // Change the logo for the person's face
        $('#navLogoImg').html('<i class="fa fa-user fa-1x"></i>');

        return this;
    }
};

// Add to cala
//Cala.Custom = Custom;
jQuery.extend(Cala, Custom);
