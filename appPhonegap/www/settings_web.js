/*******************************************************************************
 *
 * Cala
 *
 * Production settings
 *
 */

Cala.frontPage = {def: 'bhavana/default', app: 'bhavana/default_app',};

// What device is this? Android|iOs|Comp
// Set the correct option for internal storage and other minor aspects
// @todo this will be autodetected; http://www.javascripter.net/faq/operatin.htm
Cala.Device = 'comp';

// Current IP, this will work only if you are running from the same server, but not in apps
// So, I will assume that you are hosting the api in the same server, but change it if this
// is not the case.
// @todo I do not think this is valid any more, I am pretty sure I have solved this issue
// This is not even been used!!!
Cala.ip = "https://lasangha.org/meditacion/";

// Is this an embeded application? As part of a CMS or similar?
// I am not sure if this is still valid actually
//Cala.embeded = true;

// Debug?
Cala.debug = false;

/*******************************************************************************
 *
 * jDrupal
 *
 * @deprecated ?
 *
 */
// Set the site path (without the trailing slash). 
//Drupal.settings.site_path = "http://" + Cala.ip;
//Drupal.settings.site_path = Cala.ip;

// Set the Service Resource endpoint path.
//Drupal.settings.endpoint = "rest";

fullHostName = Cala.ip

/*******************************************************************************
 *
 * Cala PHP Api
 * You can use this instead of jDrupal
 * if Cala_apiUrl != false, then I will use this
 *
 */

Cala_apiUrl      = false;
Cala_basePath    = false;
Cala_IAM         = false;
Cala_SESSION_KEY = false;

