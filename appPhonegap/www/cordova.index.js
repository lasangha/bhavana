var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    onDeviceReady: function() {
        app.receivedEvent('The device is ready to rumble!');
        //Cala.connectToMainServer();
        //Cala._boot();
        //Cordova_boot();
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        Cala.say('Received Event: ' + id);
    },
};

/**
 * This is what you should call from your pages, call it at the bottom
 */
function Cordova_boot(){
    Cala.say("Booting from cordova");
    Cala_initMe();
}


