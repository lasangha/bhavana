/*******************************************************************************
 *
 * Cala
 *
 * Development settings, you should NEVER see this in production
 *
 */

Cala.frontPage = {def: 'bhavana/default', app: 'bhavana/default_app',};

// What device is this? Android|iOs|Comp
// Set the correct option for internal storage and other minor aspects
// @todo this will be autodetected; http://www.javascripter.net/faq/operatin.htm
Cala.Device = 'comp';

// Current IP, this will work only if you are running from the same server, but not in apps
// So, I will assume that you are hosting the api in the same server, but change it if this
// is not the case.
Cala.ip = "http://bhavana.lasangha.org/";
Cala.ip = "https://localhost/b2/";
Cala.ip = "https://192.168.43.186/b2/";

// Is this an embeded application? As part of a CMS or similar?
// I am not sure if this is still valid actually
//Cala.embeded = true;

// Debug?
Cala.debug = true;

/*******************************************************************************
 *
 * jDrupal
 *
 */
// Set the site path (without the trailing slash). 
//Drupal.settings.site_path = "http://" + Cala.ip;
//Drupal.settings.site_path = Cala.ip;

// Set the Service Resource endpoint path.
//Drupal.settings.endpoint = "rest";

fullHostName = Cala.ip;

/*******************************************************************************
 *
 * Cala PHP Api
 * You can use this instead of jDrupal
 * if Cala_apiUrl != false, then I will use this
 *
 */

Cala_apiUrl      = false;
Cala_basePath    = false;
Cala_IAM         = false;
Cala_SESSION_KEY = false;

/*

//Cala.frontPage = {def: 'bhavana/default', app: 'bhavana/default_app',};
Cala.frontPage = {def: 'bhavana/default', app: 'bhavana/default'};

// What device is this? Android|iOs|Comp
// Set the correct option for internal storage and other minor aspects
// @todo this will be autodetected; http://www.javascripter.net/faq/operatin.htm
Cala.Device = 'comp';

// Should I debug with weinre? This will only apply for app installs
//Cala.debugWeinre = "http://192.168.1.13:8087/";

// Current IP, this will work only if you are running from the same server, but not in apps
// So, I will assume that you are hosting the api in the same server, but change it if this
// is not the case.
//Cala.ip = "192.168.1.28/bServer";
//Cala.ip = "192.168.1.7";
//Cala.ip = "lasangha.org";

// Is this an embeded application? As part of a CMS or similar?
// I am not sure if this is still valid actually
Cala.embeded = true;

// Debug?
Cala.debug = true;

// Dinamic settings
protocol = "http://";

Cala.say("Setting correct host for web version");
if(window.location.protocol == "https:"){
    protocol = "https://"
}

hostName = window.location.hostname;

if(window.location.hostname == "meditacion.lasangha.org"){
    hostName = window.location.hostname + "/api";
}else{
    hostName = window.location.hostname + "/b2";
}

if(window.location.hostname == ""){
	hostName = "192.168.43.186";
	Cala.say("tons");
}

fullHostName = protocol + hostName;

fullHostName = "http://bhavana.lasangha.org";

Cala.say("Full host is: " + protocol + hostName);
*/

/*******************************************************************************
 *
 * jDrupal
 *
 */
// Set the site path (without the trailing slash). 
//Drupal.settings.site_path = "http://" + Cala.ip + "/bServer";
//Drupal.settings.site_path = "https://" + Cala.ip;

// /Drupal.settings.site_path = protocol + hostName + "";

// Set the Service Resource endpoint path.
// /Drupal.settings.endpoint = "rest";

/*******************************************************************************
 *
 * Cala PHP Api
 * You can use this instead of jDrupal
 * if Cala_apiUrl != false, then I will use this
 *
 */
/*
Cala_apiUrl      = false;
Cala_basePath    = false;
Cala_IAM         = false;
Cala_SESSION_KEY = false;
*/
