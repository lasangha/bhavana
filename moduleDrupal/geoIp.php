<?php

# http://dev.maxmind.com/geoip/geoip2/geolite2/#Downloads
# https://github.com/maxmind/GeoIP2-php/releases

require 'geoIp/vendor/autoload.php';
use GeoIp2\Database\Reader;

/**
 * Get location of a visitor
 */
function _geoloc_getMeVisitorDetails(){

    $ip = $_SERVER['REMOTE_ADDR'];

    $reader = new Reader(drupal_get_path('module', 'bhavana') . '/geoIp/GeoLite2-City.mmdb');

    $dets = array();

    try {
        $record = $reader->city($ip);
        $dets["longitude"] = $record->location->longitude;
        $dets["latitude"]  = $record->location->latitude;
    } catch (Exception $e) {
        $dets["longitude"] = 0;
        $dets["latitude"]  = 0;
    }

    return $dets;
}

/*
$ip = $_SERVER['REMOTE_ADDR'];

$reader = new Reader('geoIp/GeoLite2-City.mmdb');

$dets = array();

try {
    $record = $reader->city($ip);
    $dets["longitude"] = $record->location->longitude;
    $dets["latitude"]  = $record->location->latitude;

} catch (Exception $e) {
    $dets["longitude"] = 0;
    $dets["latitude"]  = 0;
    //echo 'Caught exception: ',  $e->getMessage(), "\n";
}

print_r($dets);

 */
