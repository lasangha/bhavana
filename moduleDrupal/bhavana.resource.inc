<?php

#require 'geoIp/geoip2.phar';
#use GeoIp2\Database\Reader;

global $emotionsListPos, $emotionsListNeg, $emotionsListFull;

# Emotions list
$emotionsListPos = array(
    'eno' => 'Enojo',
    'fru' => 'Frustración',
    'imp' => 'Impaciencia',
    'dep' => 'Depresión',
    'tri' => 'Tristeza',
    'ins' => 'Inseguridad',
    'ans' => 'Ansiedad',
    'pre' => 'Preocupación');
$emotionsListNeg = array(
    'gen' => 'Generosidad',
    'amo' => 'Amor',
    'com' => 'Compasión',
    'emp' => 'Empatía',
    'fel' => 'Felicidad',
    'agr' => 'Agradecimiento',
    'sat' => 'Satisfacción',
    'esp' => 'Esperanza');

$emotionsListFull = array_merge($emotionsListPos, $emotionsListNeg);

/**
 * Get location of a visitor
 */
function _geoloc_getMeVisitorDetails2(){

    $ip = $_SERVER['REMOTE_ADDR'];
    //$ip = '201.191.255.156';
    $reader = new Reader(drupal_get_path('module', 'bhavana') . '/geoIp/GeoLite2-City.mmdb');

    $dets = array();

    try {
        $record = $reader->city($ip);
        $dets["longitude"] = $record->location->longitude;
        $dets["latitude"]  = $record->location->latitude;

    } catch (Exception $e) {
        $dets["longitude"] = 0;
        $dets["latitude"]  = 0;
    }

    return $dets;
}

/**
 * Gets max time meditated in one cause and returns the json of it
 */
function bhavana_causesGetMaxTime(){
	drupal_add_http_header('Content-Type', 'text/json');
	print json_encode(_bhavana_causesGetMaxTime());
	drupal_exit();
}


/**
 * Helper
 */
function _bhavana_causesGetMaxTime(){

	$q = "SELECT idCause, name, totalTime FROM {bhavana_causes} WHERE idCause > 1 ORDER BY totalTime DESC LIMIT 1";

	$maxCause = db_query($q)->fetchAssoc();

	return $maxCause;

}

/**
 * Registers meditations times
 */
function bhavana_meditationAddTime($totalTime, $causeCode, $privacy, $where, $ref) {

	global $user;

	# I will only load the cause code if required aka not 'non'

	# Get the correct id of the cause
	# Standard db_query drupal way was not working, but today, nothing is working much
	$idCause = db_query("SELECT idCause FROM {bhavana_causes} WHERE code = '".$causeCode."'")->fetchField();

	# Privacy respect 1=yes, I want to participate 0=no, I do not want to participate
	if($privacy == 1){
		include_once("geoIp.php");
		# Where is this person?
		$location = _geoloc_getMeVisitorDetails();
	}else{
		$location = array("longitude" => 0, "latitude" => 0);
	}

	if($idCause > 0){
		# Register the new time
		$q = sprintf("INSERT INTO {bhavana_meditations} (`timestamp`, `totalTime`, `idCause`, `where`, `idUser`, `coordinates`, `ref`)
			VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')",
				time(),
				$totalTime,
				$idCause,
				$where,
				$user->uid,
				$location['longitude'] . "," . $location['latitude'],
				$ref
			);
		try{
			$r = db_query($q);
		}catch(Exception $e){
			//echo $e;
		}
	}

	# Update total times
	$q = sprintf("UPDATE {bhavana_causes} SET totalTime = totalTime + %s WHERE idCause = '%s'", $totalTime, $idCause);
	$r = db_query($q);

	# I will just say it worked, not much to do if it did not.
	bhavana_printBlank('SUCCESS_ALL_GOOD');
	//return 'SUCCESS_ALL_GOOD';

}

/**
 * Get group meditations times per day
 */
function bhavana_meditationsGetGroupMeditations(){

	drupal_add_http_header('Content-Type', 'application/json');
	print json_encode(_bhavana_meditationsGetGroupMeditations());
	drupal_exit();

}

/* Helper function */
function _bhavana_meditationsGetGroupMeditations($ini = 7){

	$q = sprintf("
		SELECT FROM_UNIXTIME(m.timestamp, '%%Y.%%e.%%m') AS day, SUM(m.totalTime) AS totalTime, m.idCause, c.name AS cName, c.code AS cCode
		FROM {bhavana_meditations} m
		INNER JOIN `bhavana_causes` c ON c.idCause = m.idCause
		WHERE timestamp > %s
		GROUP BY idCause, day
		ORDER BY day
		", (time()-($ini*86400)));

	$r = db_query($q);

	$stuff = array();
	$details = array();
	$dates  = array();

	foreach($r as $row){
		$stuff[$row->cCode][] = array("name" => $row->cName, "day" => $row->day, "totalTime" => $row->totalTime);
		$details[$row->cName]['details'][$row->day] = $row->totalTime;
		$dates[] = $row->day;
	}

	# Remove duplicates
	$dates = array_unique($dates);

	# Lets fix the dates (remove keys)
	$nDates = array();
	foreach($dates as $d){
		$nDates[] = $d;
	}

	# Fix dates to see if there are any missing ones?
	foreach($details as $topic => $d){
		foreach($dates as $date){
			if(!array_key_exists($date, $details[$topic]['details'])){
				$details[$topic]['details'][$date] = 0;
			}
		}
		# Sort them out
		ksort($details[$topic]['details']);
	}

	# Now, lets get rid of all that is not needed
	$justTheNumbers = array();
	foreach($details as $topic => $d){
		foreach($dates as $date){
			$justTheNumbers[$topic][] = $details[$topic]['details'][$date];
		}
	}

	drupal_add_http_header('Content-Type', 'application/json');
	print json_encode(array('details' => $justTheNumbers, 'dates' => $nDates));
	drupal_exit();

}

/**
 * Get the medatation locations in order to create a map
 */
function bhavana_meditationsGetLocations(){

	$q = "SELECT coordinates, timestamp
		FROM `bhavana_meditations`
		WHERE timestamp > " . (time() - 1296000) . "
		AND coordinates != ''
		AND coordinates != ',' 
		GROUP BY coordinates
		ORDER BY timestamp DESC
		LIMIT 0, 60";

	$res = db_query($q);

	$dats = array();
	foreach($res as $record){
		$dats[] = sprintf('{"type":"Feature","id":"%s","properties":{"name":"90"},"geometry":{"type":"Point","coordinates":[%s]}}',
			$record->timestamp,
			$record->coordinates);
	}

	$dats = implode(",", $dats);

	return '{"type":"FeatureCollection","features":['.$dats.']}';

}

/**
 * Retrieves the meditation times, per day, for this person
 */
function bhavana_meditationsGetMyTimes($ini = 7){

	global $user;

	$q = sprintf("
		SELECT FROM_UNIXTIME(timestamp, '%%Y.%%e.%%m') AS day, SUM(totalTime) AS totalTime
		FROM {bhavana_meditations}
		WHERE idUser = '%s'
		AND timestamp > %s
		GROUP BY day
		ORDER BY day
		", $user->uid, (time()-($ini*86400)));

	$res = db_query($q);
	$labels = array();
	$times = array();
	if($res > 0){
		foreach($res as $r){
			$labels[] = $r->day;
			$times[] = $r->totalTime;
		}
	}

	return array("labels" => $labels, "times" => $times);

}

/**
 * @brief Gets my meditation times grouped by cause
 *
 * @return Required information to create a pie chart with chart.js
 **/
function bhavana_meditationsGetMyTimesPerCause(){

	global $user;

	$q = sprintf("
		SELECT SUM(m.totalTime) AS totalTime,
			c.name AS causeName, c.code AS causeCode
			FROM {bhavana_meditations} m
			INNER JOIN {bhavana_causes} c on c.idCause = m.idCause
			WHERE idUser = %s
			GROUP BY m.idCause
			", $user->uid);

	$results = db_query($q);

	$data = array();

	# Colours
	$colours['non'] = "#E74D1E";
	$colours['paz'] = "#E4DF4D";
	$colours['com'] = "#4DE45A";
	$colours['hum'] = "#4DC1E4";

	foreach($results as $row){
		$data[] = array(
			'value' => $row->totalTime,
			'label' => $row->causeName,
			'color' => $colours[$row->causeCode]
		);
	}

	return $data;

}

/**
 * Checkin: register
 */
function bhavana_checkinRegister($emotionCode, $where, $emotionType, $mindState, $message){

	global $user;

	$q = sprintf("INSERT INTO {bhavana_checkin} (`uid`, `timestamp`, `date`, `emotionCode`, `emotionType`, `mindState`, `message`, `where`)
		VALUES('%s','%s','%s','%s','%s','%s','%s','%s')",
			$user->uid, time(), format_date(time(), 'custom', 'Ymd'), $emotionCode, $emotionType, $mindState, $message, $where);

	db_query($q);

	return true;

}

/**
 * Get the information for the check in
 */
function bhavana_checkinGetData($period){

	global $user, $emotionsListFull;

	# Period
	$period = time() - (86400 * $period);

	## Principal emotions for the period
	$emotions = array();

	$q = sprintf("SELECT count(c.emotionCode) AS count, emotionCode, emotionType, CONVERT_TZ(FROM_UNIXTIME(timestamp, '%%Y-%%m-%%e'),'GMT','%s') AS theDate
		FROM `bhavana_checkin` c
		WHERE timestamp > %s
		AND uid = %s
		GROUP BY theDate, emotionCode
		ORDER BY count DESC
		LIMIT 5
		", $user->timezone, $period, $user->uid);

	$r = db_query($q);

	$emotionsPos = 0;
	$emotionsNeg = 0;

	foreach($r as $row){
		$emotions[$emotionsListFull[$row->emotionCode]]['count'] = $row->count;
		if($row->emotionType == 'n'){
			$emotions[$emotionsListFull[$row->emotionCode]]['type'] = 'n';
			$emotionsNeg += $row->count;
		}else{
			$emotions[$emotionsListFull[$row->emotionCode]]['type'] = 'p';
			$emotionsPos += $row->count;
		}
	}

	# If I did not find anything here, I will just stop
	if(count($emotions) == 0){
		return false;
	}

	// Happiness total for the period
	//$totalsForWeek['happiness'] = round($emotionsPos/($emotionsNeg + $emotionsPos) * 100);

	//SELECT AVG(c.mindState) AS mindState, CONVERT_TZ(FROM_UNIXTIME(timestamp, '%%Y-%%m-%%e'),'GMT','%s') AS theDate $user->timezone,
	## Total happines for each day, based on how you feel
	$q = sprintf("
		SELECT AVG(c.mindState) AS mindState, FROM_UNIXTIME(timestamp, '%%Y-%%m-%%e') AS theDate
		FROM `bhavana_checkin` c
		WHERE timestamp > %s
		AND uid = %s
		GROUP BY theDate
		ORDER BY theDate DESC
		LIMIT 7
		", $period, $user->uid);

	$r = db_query($q);

	$mindStates = array();

	foreach($r as $row){
		$mindStates[substr($row->theDate, 0, 10)] = $row->mindState;
	}

	## Emotions for each day
	// @deprecated
	/*
	$q = sprintf("SELECT count(c.emotionType) AS count, emotionType, CONVERT_TZ(FROM_UNIXTIME(timestamp, '%%Y-%%m-%%e'),'GMT','%s') AS theDate
		FROM `bhavana_checkin` c
		WHERE timestamp > %s
		AND uid = %s
		GROUP BY theDate, emotionType
		ORDER BY theDate ASC, count DESC", $user->timezone, $period, $user->uid);

	$r = db_query($q);

	$totalsForEachDay = array();

	foreach($r as $row){
		// For some reason if I use array_unique the final json adds a key to each one of this
		$date = substr($row->theDate, 0, 10);
		if(!in_array($date, $totalsForEachDay['labels'])){
			$totalsForEachDay['labels'][] = $date;
		}
		if($row->emotionType == 'n'){
			$totalsForEachDay['dates'][$row->theDate]['emotionsNeg'] += $row->count;
		}else{
			$totalsForEachDay['dates'][$row->theDate]['emotionsPos'] += $row->count;
		}
	}

	foreach($totalsForEachDay['dates'] as $dates => $date){
		$totalsForEachDay['data'][] = round($date['emotionsPos']/($date['emotionsNeg'] + $date['emotionsPos']) * 100);
	}
	 */

	## Latest messages
	$q = sprintf("SELECT message
		FROM `bhavana_checkin` c
		WHERE uid = %s
		AND message != ''
		ORDER BY timestamp DESC
		LIMIT 7", $user->uid);

	$r = db_query($q);

	$latestMessages = array();
	foreach($r as $row){
		$latestMessages[] = $row->message;
	}


	return array('emotions' => $emotions,
		'totalsForEachDay' => array(
			'labels' => array_reverse(array_keys($mindStates)),
			'data' => array_reverse(array_values($mindStates)),
		),
		'latestMessages' => $latestMessages,
		'mindStates' => $mindStates
	);

	return true;

}

// View my stats
function bhavana_viewMyStats(){

	// Add js
	drupal_add_js(drupal_get_path('module', 'bhavana') . "/js/bhavana.js");
	drupal_add_js(drupal_get_path('module', 'bhavana') . "/js/Chart.min.js");

	// Get my times
	$times = bhavana_meditationsGetMyTimes();

	if(count($times['times']) > 0){
		//$createChart = "Bhavana_createChartMyStats('Meditación por día', 'Bhavana_chartTotalPerDayMe', [" . implode(',', $times['labels']) . "], [" . implode(',', $times['times']) . "]);";
		$createChart = "
			jQuery(document).ready(function(){
				Bhavana_createChartMyStats('Meditación por día', 'Bhavana_chartTotalPerDayMe', " . json_encode($times['labels']) . ", " . json_encode($times['times']) . ");
	});";
	}else{
		$createChart = '
			jQuery(document).ready(function() {
				jQuery("#Bhavana_chartTotalPerDayMeDiv").html("<div role=\'alert\' class=\'alert alert-warning\'>No hay resultados en este momento.</div>");
	});';
	}

	// Get my times per cause
	$timesPerCause = bhavana_meditationsGetMyTimesPerCause();

	if(count($timesPerCause) > 0){

		$createChartCause = '
			jQuery(document).ready(function(){
				//times = '.json_encode($timesPerCause).';
				Bhavana_printMyMeditationTimesPerCause('.json_encode($timesPerCause).');
				//function Bhavana_printMyMeditationTimesPerCause(times);
	});';
	}else{
		$createChartCause = '
			jQuery(document).ready(function() {
				jQuery("#Bhavana_chartTotalPerDayMeDiv").html("<div role=\'alert\' class=\'alert alert-warning\'>No hay resultados en este momento.</div>");
	});';
	}

	drupal_add_js($createChart, 'inline');
	drupal_add_js($createChartCause, 'inline');

	$text = '
		<div class="panel panel-info">
		<div class="panel-heading">
		<h2>Meditación total diaria</h2>
		</div>
		<div class="panel-body">
		<div id="Bhavana_chartTotalPerDayMeDiv"></div>
		<canvas id="Bhavana_chartTotalPerDayMe" height="100%"></canvas>
		</div>
		</div>

		<div class="panel panel-info">
		<div class="panel-heading">
		<h2>Mis causas</h2>
		</div>
		<div class="panel-body">
		<canvas id="chartPiePerCause"></canvas>
		<div id="Bhavana_meditationTimesPerCauseLegend"></div>
		</div>
		</div>';

	return $text;
}

// Send to the user page
function bhavana_userPage(){
	drupal_goto('user',  array('bhavana' => 'bhavana'));
}
