// Creates a chart
function Bhavana_createChartMyStats(chartTitle, canvasId, chartLabels, chartData){

	console.log("There are some meditations: " + chartLabels.length + canvasId);

	var lineChartData = {
		labels : chartLabels,
		datasets : [
		{
			label: chartTitle,
			fillColor: "rgba(76,153,0,0.2)",
			strokeColor: "rgba(76,153,0,1)",
			pointColor: "rgba(76,153,0,1)",
			pointStrokeColor : "#fff",
			pointHighlightFill : "#fff",
			pointHighlightStroke: "rgba(76,153,0,1)",
			data : chartData
		}
		]
	};

	var ctx = document.getElementById(canvasId).getContext("2d");
	window.myLine = new Chart(ctx).Line(lineChartData, {responsive: true});
}

// Get my meditation times per cause
function Bhavana_printMyMeditationTimesPerCause(response){

	console.log("Lets get the meditation chart per cause" + JSON.stringify(response));

	var options = {legendTemplate: "<ul class=\"list-group <%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li class=\"list-group-item\"><span style=\"background-color:<%=segments[i].fillColor%>\">&nbsp;<%=segments[i].value%>&nbsp;</span>&nbsp;<%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"};
	var ctx = document.getElementById('chartPiePerCause').getContext('2d');
	var perCausePieChart = new Chart(ctx).Pie(response, options);
	document.getElementById("Bhavana_meditationTimesPerCauseLegend").innerHTML = perCausePieChart.generateLegend();
}
