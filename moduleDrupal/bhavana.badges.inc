<?php

# A series of badges for the user :)

# List of meditation badges
global $bhavana_badges;
$bhavana_badges = array();
$bhavana_badges['medFirst']= array(
        'title' => 'Primera meditación',
        'desc'   => 'Ya llevó a cabo su primera meditación',
        'code'   => 'medFirst',
        'func'   => 'bhavana_badgesFirstMeditation',
        'status' => 0,
        'type'   => 'meditation',
        'period' => 'life'
    );
// Daily

$bhavana_badges['medTodayOnce']= array(
        'title'  => 'Meditó el día de hoy',
        'desc'   => 'Hoy ya llevó a cabo la meditación',
        'code'   => 'medTodayOnce',
        'func'   => 'bhavana_badgesMedTodayOnce',
        'status' => 0,
        'type'   => 'meditation',
        'period' => 'day'
);

$bhavana_badges['medTodayIntention']= array(
        'title'  => 'Meditó con una intención',
        'desc'   => '',
        'code'   => 'medTodayIntention',
        'func'   => 'bhavana_badgesMedTodayIntention',
        'status' => 0,
        'type'   => 'meditation',
        'period' => 'day'
);

$bhavana_badges['checkinToday']= array(
        'title'  => 'Registró sus emociones',
        'desc'   => '',
        'code'   => 'checkinToday',
        'func'   => 'bhavana_badgesCheckinToday',
        'status' => 0,
        'type'   => 'meditation',
        'period' => 'day'
);

// Week
$bhavana_badges['medWeekOnce']= array(
        'title'  => 'Meditó esta semana',
        'desc'   => 'Hizo una meditación esta semana',
        'code'   => 'medWeekOnce',
        'func'   => 'bhavana_badgesMedWeekOnce',
        'status' => 0,
        'type'   => 'meditation',
        'period' => 'week'
);

$bhavana_badges['checkinWeek']= array(
        'title'  => 'Registró sus emociones',
        'desc'   => '',
        'code'   => 'checkinWeek',
        'func'   => 'bhavana_badgesCheckinWeek',
        'status' => 0,
        'type'   => 'meditation',
        'period' => 'week'
);

// Month
$bhavana_badges['medMonthOnce']= array(
        'title'  => 'Meditó este mes',
        'desc'   => '',
        'code'   => 'medMonthOnce',
        'func'   => 'bhavana_badgesMedMonthOnce',
        'status' => 0,
        'type'   => 'meditation',
        'period' => 'month'
);

# This is the main function to call to parse/look for new badges
function bhavana_badgesParseThemAll($which = 'meditation'){

    if($which == 'meditation'){
        return bhavana_badgesMeditationLookForAll('meditation');
    }
}

# Gets a list of badges for this user
function bhavana_badgesGetAll($status = 1){

    global $user;

    $and = '';

    if($status == 1){
        $and = " AND (validUntil > " . time() . " OR validUntil = 0)";
    }

    # Select from db and check if there are any meditations for this person, if so, lets give it to them!
    $q = sprintf("SELECT * FROM `bhavana_badges` WHERE idUser = %s %s", $user->uid, $and);

    $r = db_query($q);

    $badges = array();

    foreach($r as $row){
        $badges[$row->badgeCode] = $row;
    }

    return $badges;

}

# Look for all possible badges in meditation
function bhavana_badgesMeditationLookForAll($which = false){

    global $user, $bhavana_badges;

    # Get all active badges
    $allActiveBadges = bhavana_badgesGetAll(1);

    # Loop all possible badges
    foreach($bhavana_badges as $b => $bb){
        # Is is valid?
        if(isset($allActiveBadges[$b])){
            # Set as active
            $bhavana_badges[$b]['status'] = 1;
        }else{    
            # look for it
            $status = call_user_func($bhavana_badges[$b]['func']);
            if($status ==  true){
                $bhavana_badges[$b]['status'] = 1;
            }
        }
    }

    return $bhavana_badges;
}

# Insert new badges
function bhavana_badgesInsertNew($dets){

    global $user;

    # Get the expiration date
    if($dets['period'] ==  'day'){
        $validUntil = time() + 86400;
    }elseif($dets['period'] == 'week'){
        $validUntil = time() + 604800;
    }elseif($dets['period'] == 'month'){
        $validUntil = time() + 2678400;
    }else{
        $validUntil = 0;
    }

    $q = sprintf("INSERT INTO `bhavana_badges` (`idUser`, `badgeCode`, `timestamp`, `validUntil`) values('%s','%s','%s','%s')",
        $user->uid, $dets['code'], time(), $validUntil);

    db_query($q);

}

# Did you do your first meditation?
function bhavana_badgesFirstMeditation(){

    global $user, $bhavana_badges;

    # Select from db and check if there are any meditations for this person, if so, lets give it to them!
    $q = sprintf("SELECT totalTime FROM `bhavana_meditations` WHERE idUser = %s", $user->uid);

    $r = db_query($q);

    # If there is at least one result, this is valid
    foreach($r as $row){
        # Insert into db
        bhavana_badgesInsertNew($bhavana_badges['medFirst']);
        return true;
    }
    return false;

}

/**********************************************************************
 * Daily badges
 */
# Did you meditate today?
function bhavana_badgesMedTodayOnce(){

    global $user, $bhavana_badges;

    # Select from db and check if there are any meditations for this person, if so, lets give it to them!
    $q = sprintf("SELECT totalTime FROM `bhavana_meditations` WHERE idUser = %s AND timestamp > %s", $user->uid, strtotime("yesterday midnight"));

    $r = db_query($q);

    # If there is at least one result, this is valid
    foreach($r as $row){
        # Insert into db
        bhavana_badgesInsertNew($bhavana_badges['medTodayOnce']);
        return true;
    }
    return false;

}

# Did you meditate with an intention today?
function bhavana_badgesMedTodayIntention(){
    return bhavana_badgesWithIntention(1);
}

# Did you check in today?
function bhavana_badgesCheckinToday(){
    return bhavana_badgesCheckin(1);
}

/**********************************************************************
 * Weekly badges
 */
# Did you meditate this week
function bhavana_badgesMedWeekOnce(){

    global $user, $bhavana_badges;

    # Select from db and check if there are any meditations for this person, if so, lets give it to them!
    $q = sprintf("SELECT totalTime FROM `bhavana_meditations` WHERE idUser = %s AND timestamp > %s", $user->uid, strtotime("last sunday midnight"));

    $r = db_query($q);

    # If there is at least one result, this is valid
    foreach($r as $row){
        # Insert into db
        bhavana_badgesInsertNew($bhavana_badges['medWeekOnce']);
        return true;
    }
    return false;

}

# Did you check in today?
function bhavana_badgesCheckinWeek(){
    return bhavana_badgesCheckin(7);
}

/**********************************************************************
 * Monthy badges
 */
# Did you meditate this month
function bhavana_badgesMedMonthOnce(){

    global $user, $bhavana_badges;

    # Select from db and check if there are any meditations for this person, if so, lets give it to them!
    $q = sprintf("SELECT totalTime FROM `bhavana_meditations` WHERE idUser = %s AND timestamp > %s", $user->uid, strtotime("last day of last month midnight"));

    $r = db_query($q);

    # If there is at least one result, this is valid
    foreach($r as $row){
        # Insert into db
        bhavana_badgesInsertNew($bhavana_badges['medMonthOnce']);
        return true;
    }
    return false;

}

/**********************************************************************
 * General functions
 */
function bhavana_badgesCheckin($period){

    global $user, $bhavana_badges;

    # Period
    $time = time() - (86400 * $period);

    $q = sprintf("SELECT count(c.emotionCode) AS count
        FROM `bhavana_checkin` c
        WHERE timestamp > %s
        AND uid = %s
        LIMIT 1
        ", $time, $user->uid);

    $r = db_query($q);

    foreach($r as $row){
        $count = $row->count;
    } 

    if($count > 0){
        if($period == 1){
            bhavana_badgesInsertNew($bhavana_badges['checkinToday']);
        }else{ 
            bhavana_badgesInsertNew($bhavana_badges['checkinWeek']);
        }
        return true;
    }else{
        return false;
    }
}

# Meditations with an intention
function bhavana_badgesWithIntention($period){

    global $user, $bhavana_badges;

    # Period
    $time = time() - (86400 * $period);

    $q = sprintf("
        SELECT SUM(m.totalTime) AS totalTime
        FROM {bhavana_meditations} m
        INNER JOIN {bhavana_causes} c on c.idCause = m.idCause
        WHERE idUser = %s
        AND m.timestamp > %s
        ", $user->uid, $time);

    $results = db_query($q);

    foreach($results as $row){
        bhavana_badgesInsertNew($bhavana_badges['medMonthOnce']);
        return true;
    }

    return false;
}

