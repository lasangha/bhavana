<?php

function bhavana_services_resources() {
    $resources = array(
        'bhavana_resources' => array(
            'actions' => array(
                'meditation_add_times' => array(
                    'help' => t('Adds meditations sessions times.'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_meditationAddTime',
                    'args' => array(
                        array(
                            'name'         => 'totalTime',
                            'type'         => 'text',
                            'description'  => t('Meditated Time'),
                            'source'       => array('data' => 'totalTime'),
                            'optional'     => FALSE,
                        ),
                        array(
                            'name'         => 'causeCode',
                            'type'         => 'char',
                            'description'  => t('Code of the cause'),
                            'source'       => array('data' => 'causeCode'),
                            'optional'     => FALSE,
                        ),
                        array(
                            'name'         => 'privacy',
                            'type'         => 'int',
                            'description'  => t('Privacy setting'),
                            'source'       => array('data' => 'privacy'),
                            'optional'     => FALSE,
                        ),
                        array(
                            'name'         => 'where',
                            'type'         => 'text',
                            'description'  => t('Where did the medatation take place'),
                            'source'       => array('data' => 'where'),
                            'optional'     => FALSE,
                        ),
                        array(
                            'name'         => 'ref',
                            'type'         => 'text',
                            'description'  => t('Reference like timer or a youtube video'),
                            'source'       => array('data' => 'ref'),
                            'optional'     => FALSE,
                        )),

                    'access callback' => 'user_access',
                    'access arguments' => array('register meditation times'),
                    'access arguments append' => false,
                ), // /meditation_add_times
                'causes_get_max_time' => array(
                    'help' => t('Gets the meditation with max times set'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_causesGetMaxTime',
                    'args' => array(),
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => false,
                ), //causes_get_max_time
                'meditation_get_group_times' => array(
                    'help' => t('Gets the meditation times for everybody'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_meditationsGetGroupMeditations',
                    'args' => array(
                        array(
                            'name'         => 'ini',
                            'type'         => 'int',
                            'description'  => t('When should I start?'),
                            'source'       => array('data' => 'ini'),
                            'optional'     => true,
                        )),
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => false,
                ), //meditation_get_group_times
                'meditation_get_map' => array(
                    'help' => t('Gets the list of locations for the meditation to put on a map'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_meditationsGetLocations',
                    'args' => array(
                        array(
                            'name'         => 'ini',
                            'type'         => 'int',
                            'description'  => t('When should I start?'),
                            'source'       => array('data' => 'ini'),
                            'optional'     => true,
                        )),
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => false,
                ), //meditation_get_map
                'meditation_get_my_times_per_day' => array(
                    'help' => t('Gets my meditations times per day'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_meditationsGetMyTimes',
                    'args' => array(
                        array(
                            'name'         => 'ini',
                            'type'         => 'int',
                            'description'  => t('When should I start?'),
                            'source'       => array('data' => 'ini'),
                            'optional'     => true,
                        )),
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => false,
                ), //meditation_get_my_times_per_day
                'meditation_get_my_times_per_cause' => array(
                    'help' => t('Gets my meditations times per cause'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_meditationsGetMyTimesPerCause',
                    'args' => array(),
                    'access callback' => 'user_access',
                    'access arguments' => array('access content'),
                    'access arguments append' => false,
                ), //meditation_get_my_times_per_cause
                'checkin_register' => array(
                    'help' => t('Register a new checkin'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_checkinRegister',
                    'args' => array(
                        array(
                            'name'         => 'emotionCode',
                            'type'         => 'text',
                            'description'  => t('The code of the emotion'),
                            'source'       => array('data' => 'emotionCode'),
                            'optional'     => FALSE,
                        ),
                        array(
                            'name'         => 'where',
                            'type'         => 'text',
                            'description'  => t('Where was it registered'),
                            'source'       => array('data' => 'where'),
                            'optional'     => FALSE,
                        ),
                        array(
                            'name'         => 'emotionType',
                            'type'         => 'text',
                            'description'  => t('The type of emotion: positive or negative'),
                            'source'       => array('data' => 'emotionType'),
                            'optional'     => FALSE,
                        ),
                        array(
                            'name'         => 'mindState',
                            'type'         => 'text',
                            'description'  => t('Current state of mind'),
                            'source'       => array('data' => 'mindState'),
                            'optional'     => FALSE,
                        ),
                        array(
                            'name'         => 'message',
                            'type'         => 'text',
                            'description'  => t('A message for the log'),
                            'source'       => array('data' => 'message'),
                            'optional'     => FALSE,
                        ),
                    ),
                    'access callback' => 'user_access',
                    'access arguments' => array('bhavana check in'),
                    'access arguments append' => false,
                ), //checkin_register
                'checkin_get_data' => array(
                    'help' => t('Get information about my checked in times'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.resource',
                    ),
                    'callback' => 'bhavana_checkinGetData',
                    'args' => array(
                        array(
                            'name'         => 'period',
                            'type'         => 'text',
                            'description'  => t('The period of time to get, in days'),
                            'source'       => array('data' => 'period'),
                            'optional'     => FALSE,
                        ),
                    ),
                    'access callback' => 'user_access',
                    'access arguments' => array('bhavana check in'),
                    'access arguments append' => false,
                ), //checkin_getData
                // Badges
                'badges_parse' => array(
                    'help' => t('Parses badges for the user, tries to get them'),
                    'file' => array(
                        'type' => 'inc',
                        'module' => 'bhavana',
                        'name' => 'bhavana.badges',
                    ),
                    'callback' => 'bhavana_badgesParseThemAll',
                    'args' => array(
                        array(
                            'name'         => 'which',
                            'type'         => 'text',
                            'description'  => t('Which badge are you looking for'),
                            'source'       => array('data' => 'which'),
                            'optional'     => FALSE,
                        ),
                    ),
                    'access callback' => 'user_access',
                    'access arguments' => array('register meditation times'),
                    'access arguments append' => false,
                ), //badges



            ),
        ),
    );
    return $resources;
}

